<?php if (is_active_sidebar('primary-sidebar')): ?>
    <ul class="sidebar">
        <?php dynamic_sidebar('primary-sidebar'); ?>
    </ul>
<?php endif; ?>