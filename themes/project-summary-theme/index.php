<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 */

get_header();
?>

    <div class="site-content content-padding">
        <?php
        if (have_posts()) :

            while (have_posts()) :

                the_post();
                ?>

                <article <?php post_class(); ?>>

                    <header class="entry-header">
                        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                    </header>
                    <! – .entry-header – >

                </article><! – #post-## – >

                <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if (comments_open() || get_comments_number()) :
                    comments_template();
                endif;

            endwhile;
        else :
            get_template_part('content-none');
        endif;
        ?>
    </div><! – .site-content – >
<?php

get_sidebar();
get_footer();