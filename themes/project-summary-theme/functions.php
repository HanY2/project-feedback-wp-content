<?php

register_nav_menus(array(
    'primary-header' => __('Primary Menu'),
));

function summary_sidebar()
{
    register_sidebar(array(
        'name' => __('Primary Sidebar', 'summary-theme'),
        'id' => 'primary-sidebar',
    ));
}

add_action('widgets_init', 'summary_sidebar');


function summary_bootstrap_enqueue_styles()
{
    wp_enqueue_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-icons', 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css');
    wp_enqueue_script('bootstrap-bundle', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js');
    wp_enqueue_style('style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'summary_bootstrap_enqueue_styles');

function summary_theme_setup()
{
    require_once 'theme-setup.php';
}

add_action('after_switch_theme', 'summary_theme_setup');

//add nav-link class to anchor links in header
add_filter('nav_menu_link_attributes', function ($atts) {
    $atts['class'] = "nav-link";
    return $atts;
});

//add nav-item class to li items in header, navbar-nav is added in header.php directly
add_filter('nav_menu_css_class', function ($classes) {
    $classes[] = 'nav-item';
    return $classes;
}, 10, 1);


