<?php
/**
 * The template for displaying single posts and pages.
 */

get_header(); ?>
    <div class="container mt-2">
        <?php
        while (have_posts()) :

            the_post();
            ?>

            <div <?php post_class(); ?>>

                <div class="entry-content">
                    <?php the_content(); ?>
                </div><!-- .entry-content -->
            </div><!-- #post-## -->

        <?php

        endwhile;
        ?>
    </div><!-- .site-content -->
<?php
get_sidebar();
get_footer();