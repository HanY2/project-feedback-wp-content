<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="site-header">
    <div class="navbar-dark bg-dark">
        <nav class="navbar navbar-expand-lg content-padding">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                    <?php bloginfo('name'); ?>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <?php

                    wp_nav_menu(array(
                        'theme_location' => 'primary-header',
                        'menu_class' => 'navbar-nav me-auto',
                        'container' => false,
                    ));

                    if (is_user_logged_in()) {
                        ?>
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item">
                                    <span class="navbar-text me-2">
                                        <?php echo 'Tere ' . wp_get_current_user()->display_name . '!' ?>
                                    </span>
                                <a class="btn btn-outline-danger" href="<?php echo wp_logout_url(); ?>">
                                    Välju
                                </a>
                            </li>
                        </ul>
                        <?php
                    } else {
                        ?>
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item">
                                <a class="btn btn-outline-info" href="<?php echo wp_login_url(); ?>">
                                    Sisene
                                </a>
                            </li>
                        </ul>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </nav>
    </div>
</header>
<! – .site-header – >