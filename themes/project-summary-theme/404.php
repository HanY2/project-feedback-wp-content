<?php
/**
 * The template for displaying 404 page.
 */

get_header(); ?>
    <div class="site-content">
        <article class="no-results">

            <div class="d-flex justify-content-center align-items-center" id="main">
                <h1 class="mr-3 pr-3 align-top border-right inline-block align-content-center">404</h1>
                <div class="inline-block align-middle">
                    <h2 class="font-weight-normal lead" id="desc">The page you requested was not found.</h2>
                </div>
            </div>
        </article><!-- .no-results -->
    </div><!-- .site-content -->
<?php
get_sidebar();
get_footer();