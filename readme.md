#Readme

##Requirements
Currently, the plugin requires Summary theme to work correctly.

#First use
1. Check, whether 'Summary theme' is selected;
2. Main menu should contain a link with title `Projektid`
   * It should not contain:
     * `#project_summary#`
     * `#project_summary_view#`

#TODO
1. Admin lists sorting
2. Report to admin using another plugin;
3. Rohkem täpsustav tekst summary clarificationi ja position meta kohta
4. PHDoc and more code cleanup;
5. `/projects` page: close all child elements when parent is closed;
6. get MIME from extension;
7. Custom post update failed messages, then remove updated message
8. Faiid: Title on päris nimi, file veerus download nimeline nupp
9. JPG mime

##Future

* Remove the theme requirement;
* Admins can export content;
* No templates in the code, whole front end is designed by the user;
* Minimize db requests

#Bugs
* a shit ton
