<?php
/*
Plugin Name: Remove Admin Bar
Description: Removes admin bar and admin page for non admin users.
Version: 1.0
Author: Hannes Härm
Author URI: https://www.linkedin.com/in/hannes-h2rm/
*/

/**
 * hide admin bar from non admin users
 */
function rab_remove_admin_bar()
{
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

add_action('after_setup_theme', 'rab_remove_admin_bar');

/**
 * Block wp-admin access for non-admins
 */
function rab_block_wp_admin()
{
    //except admin-post.php. POST requests are done to this page.
    if (rab_get_current_url() === admin_url('admin-post.php')) {
        return;
    }

    if (is_admin() && !current_user_can('administrator') &&
        !(defined('DOING_AJAX') && DOING_AJAX)) {
        wp_safe_redirect(home_url());
        exit;
    }
}

function rab_get_current_url(bool $ignore_query_str = false): string
{
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ($ignore_query_str) {
        $url = strtok($url, '?');
    }

    return $url;
}

add_action('admin_init', 'rab_block_wp_admin');
