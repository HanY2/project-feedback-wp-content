<?php

/*
Plugin Name: Project Summary
Description: Web App plugin for project summary forms. Requires Project Summary Theme to work properly.
Version: 1.0
Author: Hannes Härm
Author URI: https://www.linkedin.com/in/hannes-h2rm/
*/

if (!defined('SUMMARY_ABS_PATH_PLUGIN_ROOT')) {
    define('SUMMARY_ABS_PATH_PLUGIN_ROOT', ABSPATH . 'wp-content/plugins/project-summary/');
}

if (!defined('SUMMARY_REL_PLUGIN_PATH')) {
    define('SUMMARY_REL_PLUGIN_PATH', '/wp-content/plugins/project-summary/');
}

if (!defined('SUMMARY_FILE_UPLOAD_FOLDER')) {
    define('SUMMARY_FILE_UPLOAD_FOLDER', ABSPATH . 'wp-content/uploads/summary-uploads/');
}

require_once 'base/util/better-importer/better-import.php';
require_once 'require/define_post_type_names.php';

import_class(UoW::class);

global $unit_of_work;
$unit_of_work = new UoW();

require_once 'require/register-scripts.php';
require_once 'require/admin-menu.php'; //adds admin page to wp-admin
require_once 'require/user-content.php'; //Display user content
require_once 'require/register-custom-post-types.php'; //Register custom post types
require_once 'require/admin-custom-post-type-filters.php'; //Modify custom posts list filters
require_once 'require/admin-custom-post-type-column-values.php'; //Modify custom posts columns
require_once 'require/admin-custom-post-type-meta-fields.php'; //Modify custom posts edit fields
require_once 'require/register-admin-options.php'; //Registers plugin settings
import_class(Parsedown::class); //markdown parser


function summary_update_post($post_id)
{
    //Remove action, since wp_update_post and wp_insert_post calls save_post action, creating an infinite loop.

    remove_action('save_post', 'summary_update_post');
    do_action('save_post_no_loop', $post_id);
    add_action('save_post', 'summary_update_post');
}

add_action('save_post', 'summary_update_post');

function summary_init_plugin_activation()
{
    if (!current_user_can('activate_plugins')) {
        return;
    }
    require_once 'require/activate-plugin.php';
}

register_activation_hook(__FILE__, 'summary_init_plugin_activation');

/**
 * requires login to view content
 */
function summary_require_login()
{
    global $pagenow;

    if (!is_user_logged_in() && $pagenow != 'wp-login.php') {
        auth_redirect();
    }
}

add_action('wp', 'summary_require_login');

/**
 * Renames a page title to given name.
 */
function summary_rename_page_title($data): string
{
    global $post;
    global $unit_of_work;

    if ($post == null) {
        return $data;
    }

    if ($data == '#project_summary#' && get_page_by_title('#project_summary#')->ID == $post->ID) {
        if (isset($_GET['position']) && is_numeric($_GET['position'])) {
            $project = $unit_of_work->project_repository->get_first_by_position(intval($_GET['position']));

            if ($project) {
                return $project->name;
            }
        }
    }

    if ($data == '#project_summary_view#' && get_page_by_title('#project_summary_view#')->ID == $post->ID) {
        if (isset($_GET['position']) && is_numeric($_GET['position'])) {
            $position = $unit_of_work->position_meta_repository->get_first(intval($_GET['position']));

            if ($position) {
                return "$position->name - $position->project_display_name";
            }
        }
    }

    return $data;
}

add_filter('the_title', 'summary_rename_page_title');

/**
 * POST request processing
 */
function summary_summary_submit(): void
{
    require_once 'post-processing/summary-form.php';
}

add_action('admin_post_summary_summary', 'summary_summary_submit');

function summary_admin_notify(string $msg): void
{
    global $mymsg;
    $mymsg = $msg;

    function add_notice_query_var($location): string
    {
        global $mymsg;
        remove_filter('redirect_post_location', 'add_notice_query_var');
        return add_query_arg(array('msg' => $mymsg), $location);
    }

    add_filter('redirect_post_location', 'add_notice_query_var');
}

function summary_general_admin_notice(): void
{
    global $pagenow;
    if ($pagenow == 'update-core.php') {
        echo '<div class="notice notice-warning is-dismissible">
             <p><span style="font-weight: bold">Please do not update wordpress</span>, unless you know what you are doing! 
             If updating wordpress is necessary, please contact Summary plugin author!</p>
         </div>';
    }

    if (isset($_GET['msg'])) {
        ?>

        <div class="notice notice-warning is-dismissible">
            <p><?= $_GET['msg'] ?></p>
        </div>

        <?php
    }
}

add_action('admin_notices', 'summary_general_admin_notice');

function summary_delete_post_action($post_id)
{
    global $unit_of_work;

    $post_type = $unit_of_work->entity_repository->get_post_type($post_id);
    switch ($post_type) {
        case FILE_POST_TYPE:
            $unit_of_work->file_repository->delete($post_id);
            break;
    }
}

add_action('delete_post', 'summary_delete_post_action');

function summary_save_post_admin($post_id)
{
    global $unit_of_work;
    if (!is_admin()) {
        return;
    }

    $post_type = $unit_of_work->entity_repository->get_post_type($post_id);

    switch ($post_type) {
        case SUMMARY_POST_TYPE:
            summary_admin_summary_updated($post_id);
            break;
    }
}

add_action('save_post_no_loop', 'summary_save_post_admin');

function summary_admin_summary_updated($post_id)
{
    global $unit_of_work;
    $position = $unit_of_work->position_repository->get_first_by_summary($post_id);
    if ($position) {
        $unit_of_work->position_repository->summary_updated($position->id);
    }
}

function summary_template_redirect()
{
    global $unit_of_work;

    if (summary_request_is('/download') &&
        isset($_GET['file']) &&
        is_numeric($_GET['file']) &&
        $unit_of_work->file_repository->exists(intval($_GET['file']))) {

        $filename = $unit_of_work->file_repository->get_title(intval($_GET['file']));

        header("Content-type: application/x-msdownload", true, 200);
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo file_get_contents(
            SUMMARY_FILE_UPLOAD_FOLDER . $unit_of_work->file_repository->get_filename(intval($_GET['file'])),
            true
        );
        exit();
    }
}

add_action('template_redirect', 'summary_template_redirect');

function summary_edit_form_multipart_encoding(WP_Post $post)
{
    if ($post->post_type == FILE_POST_TYPE) {
        echo ' enctype="multipart/form-data"';
    }
}

add_action('post_edit_form_tag', 'summary_edit_form_multipart_encoding');

//global functions

function summary_get_link_to_page(string $page_title, $param = null, $value = null): string
{
    $link = get_permalink(get_page_by_title($page_title));

    if ($link == null) {
        throw new RuntimeException("Link not found for page with title '$page_title'!");
    }

    if ($param != null) {
        $link = summary_add_param_to_url($link, $param, $value);
    }

    return $link;
}

function summary_get_summary_view_link($position_id): string
{
    return summary_get_link_to_page('#project_summary_view#', 'position', $position_id);
}

function summary_get_summary_edit_link($position_id): string
{
    return summary_get_link_to_page('#project_summary#', 'position', $position_id);
}

function summary_request_is(string $request): bool
{
    return summary_get_clean_request_uri() == $request;
}

function summary_the_file_download_link(string $text, string $class, int $id, bool $new_tab = false): string
{
    $meta = $new_tab ? 'target="_blank" rel="noopener"' : '';
    $meta = $class ? $meta . " class='$class'" : $meta;

    $link = summary_get_file_download_link($id);

    return "<a href='$link' $meta>$text</a>";
}

function summary_get_file_download_link($id): string
{
    global $unit_of_work;

    if ($unit_of_work->file_repository->exists($id)) {
        return get_site_url() . "/download?file=$id";
    }

    return '';
}

function summary_add_param_to_url($url, $param, $value): string
{
    $query = parse_url($url, PHP_URL_QUERY);
    if ($query) {
        return $url . "&$param=$value";
    }
    return $url . "?$param=$value";
}

function summary_get_uri_of_site_base_url()
{
    return isset(parse_url(get_site_url())['path']) ? parse_url(get_site_url())['path'] : '';
}

function summary_get_clean_request_uri(bool $include_params = false)
{
    $res = $_SERVER['REQUEST_URI'];
    if (!$include_params) {
        $res = strtok($res, '?');
    }

    $base_uri = summary_get_uri_of_site_base_url();
    if ($base_uri == '') {
        return $res;
    }

    $res = rtrim($res, '/\\');
    return str_replace($base_uri, '', $res);
}

function summary_date(string $format, int $stamp): string
{
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('UTC'));
    $date->setTimestamp($stamp);
    $date->setTimezone(new DateTimeZone('Europe/Helsinki'));

    return $date->format($format);
}

function summary_log(int $row, string $data)
{
    wp_insert_post(array(
        'post_title' => 'summary_log',
        'post_content' => "$row: $data",
        'post_type' => LOG_POST_TYPE
    ));
}

function summary_log_to_file(string $data)
{
    file_put_contents(ABSPATH . 'wp-content/uploads/log.txt', $data . PHP_EOL, FILE_APPEND);
}

function summary_get_current_url(): string
{
    global $wp;
    return home_url( $wp->request );
}
