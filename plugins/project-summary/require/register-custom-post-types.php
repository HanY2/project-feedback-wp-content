<?php
function summary_custom_post_project_name()
{
    $labels = array(
        'name' => 'Project Names',
        'singular_name' => 'Project Name',
        'add_new' => 'Add new Project Name',
        'all_items' => 'All Project Names',
        'add_new_item' => 'Add new Project Name',
        'edit_item' => 'Edit Project Name',
        'new_item' => 'New Project Name',
        'view_item' => 'View Project Name',
        'search_item' => 'Search Project Name',
        'not_found' => 'No project names found',
        'not_found_in_trash' => 'No project names found in trash',
        'parent_item_colon' => 'Parent Item',
    );

    $support = array(
        'title'
    );

    $args = array(
        'can_export' => false,
        'capability_type' => 'post',
        'delete_with_user' => false,
        'description' => 'INSERT DESCRIPTION HERE',
        'exclude_from_search' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => false,
        'query_var' => false,
        'rewrite' => true,
        'show_in_menu' => false,
        'show_in_nav_menus' => false,
        'show_in_rest' => false,
        'show_ui' => true,
        'supports' => $support,
        'taxonomies' => array(),
    );

    register_post_type(PROJECT_NAME_POST_TYPE, $args);
}

function summary_custom_post_project()
{
    $labels = array(
        'name' => 'Projects',
        'singular_name' => 'Project',
        'add_new' => 'Add new Project',
        'all_items' => 'All Projects',
        'add_new_item' => 'Add new Project',
        'edit_item' => 'Edit Project',
        'new_item' => 'New Project',
        'view_item' => 'View Project',
        'search_item' => 'Search Project',
        'not_found' => 'No projects found',
        'not_found_in_trash' => 'No projects found in trash',
        'parent_item_colon' => 'Parent Item',
    );

    $support = array(
        ''
    );

    $args = array(
        'can_export' => false,
        'capability_type' => 'post',
        'delete_with_user' => false,
        'description' => 'INSERT DESCRIPTION HERE',
        'exclude_from_search' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => false,
        'query_var' => false,
        'rewrite' => true,
        'show_in_menu' => false,
        'show_in_nav_menus' => false,
        'show_in_rest' => false,
        'show_ui' => true,
        'supports' => $support,
        'taxonomies' => array(),
    );

    register_post_type(PROJECT_POST_TYPE, $args);
}

function summary_custom_post_position()
{
    $labels = array(
        'name' => 'Positions',
        'singular_name' => 'Position',
        'add_new' => 'Add new Position',
        'all_items' => 'All Positions',
        'add_new_item' => 'Add new Position',
        'edit_item' => 'Edit Position',
        'new_item' => 'New Position',
        'view_item' => 'View Position',
        'search_item' => 'Search Position',
        'not_found' => 'No positions found',
        'not_found_in_trash' => 'No positions found in trash',
        'parent_item_colon' => 'Parent Item',
    );

    $support = array(
        'title'
    );

    $args = array(
        'can_export' => false,
        'capability_type' => 'post',
        'delete_with_user' => false,
        'description' => 'INSERT DESCRIPTION HERE',
        'exclude_from_search' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => false,
        'query_var' => false,
        'rewrite' => true,
        'show_in_menu' => false,
        'show_in_nav_menus' => false,
        'show_in_rest' => false,
        'show_ui' => true,
        'supports' => $support,
        'taxonomies' => array(),
    );

    register_post_type(POSITION_POST_TYPE, $args);
}

function summary_custom_post_summary()
{
    $labels = array(
        'name' => 'Summaries',
        'singular_name' => 'Summary',
        'add_new' => 'Add new Summary',
        'all_items' => 'All Summaries',
        'add_new_item' => 'Add new Summary',
        'edit_item' => 'Edit Summary',
        'new_item' => 'New Summary',
        'view_item' => 'View Summary',
        'search_item' => 'Search Summary',
        'not_found' => 'No summaries found',
        'not_found_in_trash' => 'No summaries found in trash',
        'parent_item_colon' => 'Parent Item',
    );

    $support = array(
        'title',
    );

    $args = array(
        'can_export' => false,
        'capability_type' => 'post',
        'delete_with_user' => false,
        'description' => 'INSERT DESCRIPTION HERE',
        'exclude_from_search' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => false,
        'query_var' => false,
        'rewrite' => true,
        'show_in_menu' => false,
        'show_in_nav_menus' => false,
        'show_in_rest' => false,
        'show_ui' => true,
        'supports' => $support,
        'taxonomies' => array(),
    );

    register_post_type(SUMMARY_POST_TYPE, $args);
}

function summary_custom_post_file()
{
    $labels = array(
        'name' => 'Files',
        'singular_name' => 'File',
        'add_new' => 'Add new File',
        'all_items' => 'All Files',
        'add_new_item' => 'Add new File',
        'edit_item' => 'Edit File',
        'new_item' => 'New File',
        'view_item' => 'View File',
        'search_item' => 'Search File',
        'not_found' => 'No files found',
        'not_found_in_trash' => 'No files found in trash',
        'parent_item_colon' => 'Parent Item',
    );

    $support = array(
        ''
    );

    $args = array(
        'can_export' => false,
        'capability_type' => 'post',
        'delete_with_user' => false,
        'description' => 'INSERT DESCRIPTION HERE',
        'exclude_from_search' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => false,
        'query_var' => false,
        'rewrite' => true,
        'show_in_menu' => false,
        'show_in_nav_menus' => false,
        'show_in_rest' => false,
        'show_ui' => true,
        'supports' => $support,
        'taxonomies' => array(),
    );

    register_post_type(FILE_POST_TYPE, $args);
}

add_action('init', 'summary_custom_post_project_name');
add_action('init', 'summary_custom_post_project');
add_action('init', 'summary_custom_post_position');
add_action('init', 'summary_custom_post_summary');
add_action('init', 'summary_custom_post_file');
