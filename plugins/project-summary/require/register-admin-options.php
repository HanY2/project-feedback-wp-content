<?php

function summary_custom_settings()
{
    register_setting('summary-settings-group', 'summary_min_length');
    register_setting('summary-settings-group', 'summary_max_length');
    register_setting('summary-settings-group', 'summary_max_file_size');
    register_setting('summary-settings-group', 'summary_allowed_file_formats');
    register_setting('summary-settings-group', 'summary_summary_form_message');
    register_setting('summary-settings-group', 'summary_time_to_hide_seconds');
    register_setting('summary-settings-group', 'summary_time_to_hide_minutes');
    register_setting('summary-settings-group', 'summary_time_to_hide_hours');
    register_setting('summary-settings-group', 'summary_time_to_hide_days');
    register_setting('summary-settings-group', 'summary_time_to_remove_old_seconds');
    register_setting('summary-settings-group', 'summary_time_to_remove_old_minutes');
    register_setting('summary-settings-group', 'summary_time_to_remove_old_hours');
    register_setting('summary-settings-group', 'summary_time_to_remove_old_days');

    add_settings_section('summary-time-to-hide-options', 'Position Removal Time After Complete', 'summary_time_to_hide', 'summary_admin_menu');
    add_settings_field('summary-time-to-hide-seconds', 'Seconds', 'summary_time_to_hide_seconds', 'summary_admin_menu', 'summary-time-to-hide-options');
    add_settings_field('summary-time-to-hide-minutes', 'Minutes', 'summary_time_to_hide_minutes', 'summary_admin_menu', 'summary-time-to-hide-options');
    add_settings_field('summary-time-to-hide-hours', 'Hours', 'summary_time_to_hide_hours', 'summary_admin_menu', 'summary-time-to-hide-options');
    add_settings_field('summary-time-to-hide-days', 'Days', 'summary_time_to_hide_days', 'summary_admin_menu', 'summary-time-to-hide-options');

    add_settings_section('summary-time-to-remove-old', 'Old Project Removal Time After Complete', 'summary_time_to_remove_old', 'summary_admin_menu');
    add_settings_field('summary-time-to-remove-old-seconds', 'Seconds', 'summary_time_to_remove_old_seconds', 'summary_admin_menu', 'summary-time-to-remove-old');
    add_settings_field('summary-time-to-remove-old-minutes', 'Minutes', 'summary_time_to_remove_old_minutes', 'summary_admin_menu', 'summary-time-to-remove-old');
    add_settings_field('summary-time-to-remove-old-hours', 'Hours', 'summary_time_to_remove_old_hours', 'summary_admin_menu', 'summary-time-to-remove-old');
    add_settings_field('summary-time-to-remove-old-days', 'Days', 'summary_time_to_remove_old_days', 'summary_admin_menu', 'summary-time-to-remove-old');

    add_settings_section('summary-content-options', 'Content Options', 'summary_content_options', 'summary_admin_menu');
    add_settings_field('summary-content-min-length', 'Summary Minimum Length', 'summary_content_min_length', 'summary_admin_menu', 'summary-content-options');
    add_settings_field('summary-content-max-length', 'Summary Maximum Length', 'summary_content_max_length', 'summary_admin_menu', 'summary-content-options');
    add_settings_field('summary-summary-form-message', 'Summary Form Message', 'summary_summary_form_message', 'summary_admin_menu', 'summary-content-options');

    add_settings_section('summary-file-options', 'File Upload Options', 'summary_file_options', 'summary_admin_menu');
    add_settings_field('summary-max-file-size', 'Max Upload File Size', 'summary_max_file_size', 'summary_admin_menu', 'summary-file-options');
    add_settings_field('summary-allowed-file-formats', 'Allowed Upload File Format', 'summary_allowed_file_formats', 'summary_admin_menu', 'summary-file-options');
}

function summary_time_to_remove_old()
{
    echo '<p>Set time to remove old project after user has completed their summary.</p>';
}

function summary_time_to_remove_old_seconds()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_old_project_removal_time_seconds();

    echo "<input type='number' min='0' max='59' style='min-width: 70px; width: 5%;' name='summary_time_to_remove_old_seconds' value='$time'>";
}

function summary_time_to_remove_old_minutes()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_old_project_removal_time_minutes();

    echo "<input type='number' min='0' max='59' style='min-width: 70px; width: 5%;' name='summary_time_to_remove_old_minutes' value='$time'>";
}

function summary_time_to_remove_old_hours()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_old_project_removal_time_hours();

    echo
    "<input type='number' min='0' style='min-width: 70px; width: 5%;' name='summary_time_to_remove_old_hours' value='$time'>";
}

function summary_time_to_remove_old_days()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_old_project_removal_time_days();

    echo "<input type='number' min='0' style='min-width: 70px; width: 5%;' name='summary_time_to_remove_old_days' value='$time'>";
}


function summary_time_to_hide()
{
    echo '<p>Set time to hide users project after edit finish.</p>';
}

function summary_time_to_hide_seconds()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_project_hiding_time_seconds();

    echo "<input type='number' min='0' max='59' style='min-width: 70px; width: 5%;' name='summary_time_to_hide_seconds' value='$time'>";
}

function summary_time_to_hide_minutes()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_project_hiding_time_minutes();

    echo "<input type='number' min='0' max='59' style='min-width: 70px; width: 5%;' name='summary_time_to_hide_minutes' value='$time'>";
}

function summary_time_to_hide_hours()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_project_hiding_time_hours();

    echo
    "<input type='number' min='0' style='min-width: 70px; width: 5%;' name='summary_time_to_hide_hours' value='$time'>";
}

function summary_time_to_hide_days()
{
    global $unit_of_work;
    $time = $unit_of_work->general_repository->get_project_hiding_time_days();

    echo "<input type='number' min='0' style='min-width: 70px; width: 5%;' name='summary_time_to_hide_days' value='$time'>";
}

function summary_file_options()
{
    echo '<p>Edit file upload settings.</p>';
}

function summary_max_file_size()
{
    global $unit_of_work;
    $max_size = $unit_of_work->general_repository->get_max_upload_file_size();
    echo "<p>Input max file size in bytes. 1 MB (Megabyte) = 1048576 = 1024 x 1024 bytes.</p>
            <input id='size-input' type='number' min='0' max='268435456' name='summary_max_file_size' value='$max_size'>
            = <span id='pretty-size' style='font-weight: bold; font-style: italic'>null</span>";
}

function summary_allowed_file_formats()
{
    global $unit_of_work;
    $allowed_formats = $unit_of_work->general_repository->get_allowed_file_formats_as_string();
    echo "<p>Enter allowed formats separated by comma. Example: <strong>pdf,txt,xlsx</strong></p>
            <textarea name='summary_allowed_file_formats' style='min-width: 450px; min-height: 300px' placeholder='pdf=>application/pdf,txt=>plain/text'>$allowed_formats</textarea>";
}

function summary_content_options()
{
    echo '<p>Edit summary content settings.</p>';
}

function summary_content_min_length()
{
    global $unit_of_work;
    $min_length = $unit_of_work->general_repository->get_summary_min_length();
    echo "<input type='number' min='0' max='10000' name='summary_min_length' value='$min_length'>";
}

function summary_content_max_length()
{
    global $unit_of_work;
    $max_length = $unit_of_work->general_repository->get_summary_max_length();
    echo "<input type='number' min='0' max='10000' name='summary_max_length' value='$max_length'>";
}

function summary_summary_form_message()
{
    global $unit_of_work;
    $summary_message = $unit_of_work->general_repository->get_summary_message();
    echo "<input type='text' name='summary_summary_form_message' style='min-width: 450px;' value='$summary_message'>";
}

add_action('admin_init', 'summary_custom_settings');