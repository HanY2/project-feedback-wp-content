<?php

/**
 * Register and/or enqueue scripts for admin content
 */
function summary_admin_scripts(): void
{
    //custom post type validation
    wp_register_script('summary_project_creation_validation', SUMMARY_REL_PLUGIN_PATH . 'js/project-creation-validation.js');
    wp_register_script('summary_position_creation_validation', SUMMARY_REL_PLUGIN_PATH . 'js/position-creation-validation.js');
    wp_register_script('summary_project_name_creation_validation', SUMMARY_REL_PLUGIN_PATH . 'js/project-name-creation-validation.js');
    wp_register_script('summary_summary_creation_validation', SUMMARY_REL_PLUGIN_PATH . 'js/summary-creation-validation.js');
    wp_register_script('summary_file_creation_validation', SUMMARY_REL_PLUGIN_PATH . 'js/file-creation-validation.js');
    wp_register_script('summary_summary_meta_creation_validation', SUMMARY_REL_PLUGIN_PATH . 'js/summary-meta-creation-validation.js');

    wp_register_style('summary_project_creation_validation_styles', SUMMARY_REL_PLUGIN_PATH . 'css/project-creation-validation-error.css');

    wp_register_script('summary_admin_main_menu', SUMMARY_REL_PLUGIN_PATH . '/js/admin-main-menu-jquery-datepicker.js');

    wp_register_script('summary_format_bytes', SUMMARY_REL_PLUGIN_PATH . 'js/format-bytes.js');
    wp_register_style('summary_remove_filter_button', SUMMARY_REL_PLUGIN_PATH . 'css/remove-filter-button.css');
}

add_action('admin_enqueue_scripts', 'summary_admin_scripts');

function summary_scripts(): void
{
    wp_register_style('summary_content', SUMMARY_REL_PLUGIN_PATH . 'css/summary-content.css');
    wp_register_style('summary_list_page', SUMMARY_REL_PLUGIN_PATH . 'css/summary-list-page.css');

    wp_register_style('markdown_style', 'https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css');
    wp_register_script('markdown_editor', 'https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js');
    wp_register_script('markdown_init', SUMMARY_REL_PLUGIN_PATH . 'js/markdown-init.js');
    wp_register_script('summary_content_edit', SUMMARY_REL_PLUGIN_PATH . 'js/summary-content-edit.js');
}

add_action('wp_enqueue_scripts', 'summary_scripts');