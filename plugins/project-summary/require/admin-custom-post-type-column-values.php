<?php

import_class(Summary::class);

function summary_positions_custom_columns($columns): array
{
    return array(
        'cb' => $columns['cb'],
        'title' => __('Title'),
        'status' => 'Status',
        'project' => 'Project',
        'user' => 'User'
    );
}

function summary_project_names_custom_columns($columns): array
{
    return array(
        'cb' => $columns['cb'],
        'title' => 'Name',
    );
}

function summary_projects_custom_columns($columns): array
{
    return array(
        'cb' => $columns['cb'],
        'title' => 'Project',
    );
}

function summary_summary_custom_columns($columns): array
{
    return array(
        'cb' => $columns['cb'],
        'title' => __('Title'),
        'content_length' => 'Content Length',
        'position' => 'Position',
        'user' => 'User',
        'project' => 'Project',
    );
}

function summary_file_custom_columns($columns): array
{
    return array(
        'cb' => $columns['cb'],
        'title' => __('Title'),
        'file_link' => 'File',
        'position' => 'Position',
    );
}

global $unit_of_work;

add_filter('manage_' . POSITION_POST_TYPE . '_posts_columns', 'summary_positions_custom_columns');
add_filter('manage_' . PROJECT_POST_TYPE . '_posts_columns', 'summary_projects_custom_columns');
add_filter('manage_' . PROJECT_NAME_POST_TYPE . '_posts_columns', 'summary_project_names_custom_columns');
add_filter('manage_' . SUMMARY_POST_TYPE . '_posts_columns', 'summary_summary_custom_columns');
add_filter('manage_' . FILE_POST_TYPE . '_posts_columns', 'summary_file_custom_columns');


function summary_positions_columns($column, $post_id)
{
    global $unit_of_work;

    if ($column === 'project') {
        $project = $unit_of_work->project_repository->get_first_by_position($post_id);
        if (!$project) {
            $edit_link = get_edit_post_link($post_id);
            echo "<strong>No project selected! <a href='$edit_link'>Select project.</a></strong>";
        } else {
            echo $project->name;
        }
    }

    if ($column === 'user') {
        $user = $unit_of_work->user_repository->get_first_by_position($post_id);
        if ($user) {
            echo $user->display_name;
        } else {
            $edit_link = get_edit_post_link($post_id);
            echo "<strong>No user selected! <a href='$edit_link'>Select user.</a></strong>";
        }
    }

    if ($column == 'status') {
        if ($unit_of_work->position_repository->is_locked($post_id)) {
            $completed_date = $unit_of_work->position_repository->get_complete_date_stamp($post_id);
            $time = summary_date('d/M/Y H:i:s', $completed_date);
            echo "<span style='color: #0e7c00'>Complete, $time</span>";
        } else {
            echo '<span style="color: #7b1010">Incomplete</span>';
        }
    }
}

function summary_summary_columns($column, $post_id)
{
    global $unit_of_work;

    if ($column == 'content_length') {
        echo $unit_of_work->summary_repository->get_length($post_id);
    }

    if ($column == 'position') {
        $position = $unit_of_work->position_repository->get_first_by_summary($post_id);
        if (!$position) {
            $edit_link = get_edit_post_link($post_id);
            echo "<strong>Position not set! <a href='$edit_link'>Select position.</a></strong>";
        } else {
            echo $position->name;
        }
    }

    if ($column == 'user') {
        $position = $unit_of_work->position_meta_repository->get_first_by_summary($post_id);
        if (!$position) {
            echo '--';
        } else if ($position->user_display_name) {
            echo $position->user_display_name;
        } else {
            $edit_link = get_edit_post_link($position->id);
            echo "<strong>No user selected! <a href='$edit_link'>Select user.</a></strong>";
        }
    }

    if ($column == 'project') {
        $position = $unit_of_work->position_meta_repository->get_first_by_summary($post_id);
        if ($position) {
            if ($position->project_display_name) {
                echo $position->project_display_name;
            } else {
                $edit_link = get_edit_post_link($position->id);
                echo "<strong>No project selected! <a href='$edit_link'>Select project.</a></strong>";
            }
        } else {
            echo '--';
        }
    }
}

function summary_file_columns($column, $post_id)
{
    global $unit_of_work;

    if ($column == 'file_link') {
        $file_title = $unit_of_work->file_repository->get_title($post_id);
        if ($file_title) {
            echo
            summary_the_file_download_link(
                $file_title,
                'mx-2',
                $post_id,
                true
            );
        } else {
            echo 'No file selected';
        }
    }

    if ($column == 'position') {
        $position = $unit_of_work->position_meta_repository->get_first_by_file($post_id);
        if ($position) {
            echo "$position->name ($position->project_display_name)";
        } else {
            echo '-';
        }
    }
}

add_action('manage_' . POSITION_POST_TYPE . '_posts_custom_column', 'summary_positions_columns', 10, 2);
add_action('manage_' . SUMMARY_POST_TYPE . '_posts_custom_column', 'summary_summary_columns', 10, 2);
add_action('manage_' . FILE_POST_TYPE . '_posts_custom_column', 'summary_file_columns', 10, 2);