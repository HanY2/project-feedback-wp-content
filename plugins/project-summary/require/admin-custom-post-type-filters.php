<?php

function summary_summaries_sortable_columns($columns)
{
    $columns['content_length'] = 'content_length';
    return $columns;
}

add_filter('manage_edit-' . SUMMARY_POST_TYPE . '_sortable_columns', 'summary_summaries_sortable_columns');


/**
 * @throws Exception
 */
function summary_remove_unnecessary_filters()
{
    $screen = get_current_screen();

    switch ($screen->post_type) {
        case FILE_POST_TYPE:
        case SUMMARY_POST_TYPE:
        case POSITION_POST_TYPE:
            add_filter('months_dropdown_results', '__return_empty_array');
            break;
        case PROJECT_NAME_POST_TYPE:
        case PROJECT_POST_TYPE:
            add_filter('months_dropdown_results', '__return_empty_array');
            wp_enqueue_style('summary_remove_filter_button');
            break;
        default:
            break;
        }
}

add_action('admin_head', 'summary_remove_unnecessary_filters');

function summary_show_position_filter()
{
    global $unit_of_work;

    $projects = $unit_of_work->project_repository->get_all();
    $current_project = '';
    if (isset($_GET['project'])) {
        $current_project = $_GET['project']; // Check if option has been selected
    } ?>

    <label for="project"></label>
    <select name="project" id="project">
        <option value="all" <?php selected('all', $current_project); ?>>All Projects</option>
        <option value="-1" <?php selected('-1', $current_project); ?>>No projects</option>
        <?php foreach ($projects as $project): ?>
            <option value="<?= $project->id ?>" <?= selected($project->id, $current_project, false); ?>>
                <?= $project->name ?>
            </option>
        <?php endforeach; ?>
    </select>

    <?php
    $users = $unit_of_work->user_repository->get_all();
    $current_user = '';
    if (isset($_GET['user'])) {
        $current_user = $_GET['user']; // Check if option has been selected
    } ?>

    <label for="user"></label>
    <select name="user" id="user">
        <option value="all" <?php selected('all', $current_user); ?>>All Users</option>
        <option value="-1" <?php selected('-1', $current_user); ?>>No users</option>
        <?php foreach ($users as $user): ?>
            <option value="<?= $user->ID ?>" <?= selected($user->ID, $current_user, false); ?>>
                <?= $user->display_name ?>
            </option>
        <?php endforeach; ?>
    </select>

    <?php
}

function summary_show_summary_filter()
{
    global $unit_of_work;

    $positions = $unit_of_work->position_meta_repository->get_all();
    $current_position = '';
    if (isset($_GET['position'])) {
        $current_position = $_GET['position']; // Check if option has been selected
    } ?>

    <label for="position"></label>
    <select name="position" id="position">
        <option value="all" <?php selected('all', $current_position); ?>>All Positions</option>
        <option value="-1" <?php selected('-1', $current_position); ?>>No Positions</option>
        <?php foreach ($positions as $position): ?>
            <option value="<?= $position->id ?>" <?= selected($position->id, $current_position, false); ?>>
                <?= $position->name ?> (<?= $position->project_display_name ?>)
            </option>
        <?php endforeach; ?>
    </select>

    <?php
    $projects = $unit_of_work->project_repository->get_all();
    $current_project = '';
    if (isset($_GET['project'])) {
        $current_project = $_GET['project']; // Check if option has been selected
    } ?>

    <label for="project"></label>
    <select name="project" id="project">
        <option value="all" <?php selected('all', $current_project); ?>>All Projects</option>
        <option value="-1" <?php selected('-1', $current_project); ?>>No projects</option>
        <?php foreach ($projects as $project): ?>
            <option value="<?= $project->id ?>" <?= selected($project->id, $current_project, false); ?>>
                <?= $project->name ?>
            </option>
        <?php endforeach; ?>
    </select>

    <?php
    $users = $unit_of_work->user_repository->get_all();
    $current_user = '';
    if (isset($_GET['user'])) {
        $current_user = $_GET['user']; // Check if option has been selected
    } ?>

    <label for="user"></label>
    <select name="user" id="user">
        <option value="all" <?php selected('all', $current_user); ?>>All users</option>
        <?php foreach ($users as $user): ?>
            <option value="<?= $user->ID ?>" <?= selected($user->ID, $current_user, false); ?>>
                <?= $user->display_name ?>
            </option>
        <?php endforeach; ?>
    </select>

    <?php
}

function summary_show_filter_lists()
{
    global $typenow;
    if ($typenow == POSITION_POST_TYPE) {
        summary_show_position_filter();
    } else if ($typenow == SUMMARY_POST_TYPE) {
        summary_show_summary_filter();
    }
}

add_action('restrict_manage_posts', 'summary_show_filter_lists');

function summary_is_post_type(string $post_type, $query): bool
{
    return ($_GET['post_type'] ?? '') == $post_type &&
        isset($query->query['post_type']) &&
        $query->query['post_type'] == $post_type;
}

function summary_filter_post_list($query)
{
    global $pagenow;
    if (!is_admin() || $pagenow != 'edit.php') {
        return;
    }

    if (summary_is_post_type(POSITION_POST_TYPE, $query)) {
        summary_filter_position_list($query);
    }
    if (summary_is_post_type(SUMMARY_POST_TYPE, $query)) {
        summary_filter_summary_list($query);
    }
}

function summary_filter_summary_list($query)
{
    global $unit_of_work;
    $meta_query_vars = array();

    if (isset($_GET['status'])) {
        if ($_GET['status'] == '-1') {
            $meta_query_vars[] = array(
                'key' => 'summary_summary_status',
                'compare' => 'NOT EXISTS',
            );
        } else if ($_GET['status'] != 'all') {
            $meta_query_vars[] = array(
                'key' => 'summary_summary_status',
                'value' => $_GET['status'],
                'compare' => '=',
            );
        }
    }


    if (isset($_GET['position'])) {
        if ($_GET['position'] == '-1') {
            $meta_query_vars[] = array(
                'key' => 'summary_summary_position',
                'compare' => 'NOT EXISTS',
            );
        } else if ($_GET['position'] != 'all') {
            $meta_query_vars[] = array(
                'key' => 'summary_summary_position',
                'value' => $_GET['position'],
                'compare' => '=',
            );
        }
    }

    if (isset($_GET['project'])) {
        if ($_GET['project'] == '-1') {
            $positions = $unit_of_work->position_repository->get_position_ids_without_project();
            $meta_query_vars[] = array(
                'key' => 'summary_summary_position',
                'value' => sizeof($positions) != 0 ? $positions : array('empty'),
                'compare' => 'IN',
            );
        } else if ($_GET['project'] != 'all') {
            $positions = $unit_of_work->position_repository->get_position_ids_by_project(intval($_GET['project']));
            $meta_query_vars[] = array(
                'key' => 'summary_summary_position',
                'value' => sizeof($positions) != 0 ? $positions : array('empty'),
                'compare' => 'IN',
            );
        }
    }

    if (isset($_GET['user'])) {
        if ($_GET['user'] != 'all') {
            $positions = $unit_of_work->position_repository->get_position_ids_by_user(intval($_GET['user']));
            $meta_query_vars[] = array(
                'key' => 'summary_summary_position',
                'value' => sizeof($positions) != 0 ? $positions : array('empty'),
                'compare' => 'IN',
            );
        }
    }

    $query->set('meta_query', $meta_query_vars);
}

function summary_filter_position_list($query)
{
    $meta_query_vars = array();

    if (isset($_GET['project'])) {
        if ($_GET['project'] == '-1') {
            $meta_query_vars[] = array(
                'key' => 'summary_position_project',
                'compare' => 'NOT EXISTS',
            );
        } else if ($_GET['project'] != 'all') {
            $meta_query_vars[] = array(
                'key' => 'summary_position_project',
                'value' => $_GET['project'],
                'compare' => '=',
            );
        }
    }


    if (isset($_GET['user'])) {
        if ($_GET['user'] == '-1') {
            $meta_query_vars[] = array(
                'key' => 'summary_position_user',
                'compare' => 'NOT EXISTS',
            );
        } else if ($_GET['user'] != 'all') {
            $meta_query_vars[] = array(
                'key' => 'summary_position_user',
                'value' => $_GET['user'],
                'compare' => '=',
            );
        }
    }
    $query->set('meta_query', $meta_query_vars);
}

add_filter('parse_query', 'summary_filter_post_list');
