<?php

function summary_admin_menu_main()
{
    add_menu_page(
        'Project Summary',
        'Project Summary',
        'manage_options',
        'summary_admin_menu',
        'summary_admin_main_page',
        '',
        200);
}

function summary_admin_menu_project_names()
{
    add_submenu_page(
        'summary_admin_menu',
        'Project Names',
        'Project Names',
        'manage_options',
        'edit.php?post_type=' . PROJECT_NAME_POST_TYPE,
        NULL,
        1
    );
}

function summary_admin_menu_projects()
{
    add_submenu_page(
        'summary_admin_menu',
        'Projects',
        'Projects',
        'manage_options',
        'edit.php?post_type=' . PROJECT_POST_TYPE,
        NULL,
        2
    );
}

function summary_admin_menu_positions()
{
    add_submenu_page(
        'summary_admin_menu',
        'Positions',
        'Positions',
        'manage_options',
        'edit.php?post_type=' . POSITION_POST_TYPE,
        NULL,
        3
    );
}

function summary_admin_menu_summaries()
{
    add_submenu_page(
        'summary_admin_menu',
        'Summaries',
        'Summaries',
        'manage_options',
        'edit.php?post_type=' . SUMMARY_POST_TYPE,
        NULL,
        5
    );
}

function summary_admin_menu_files()
{
    add_submenu_page(
        'summary_admin_menu',
        'Uploaded Files',
        'Uploaded Files',
        'manage_options',
        'edit.php?post_type=' . FILE_POST_TYPE,
        NULL,
        6
    );
}

function summary_admin_main_page()
{
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/admin/plugin-main-page.php';
}


add_action('admin_menu', 'summary_admin_menu_main');
add_action('admin_menu', 'summary_admin_menu_project_names');
add_action('admin_menu', 'summary_admin_menu_projects');
add_action('admin_menu', 'summary_admin_menu_positions');
add_action('admin_menu', 'summary_admin_menu_summaries');
add_action('admin_menu', 'summary_admin_menu_files');
