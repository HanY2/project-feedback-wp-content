<?php

/**
 * Plugin activation process.
 * On plugin activation this script will run.
 */

require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/user-content/database-templates/projects-list-page.php';
require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/user-content/database-templates/summaries-form-page.php';
require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/user-content/database-templates/summaries-view-form-page.php';

function summary_init_create_file_structure()
{
    if (!file_exists(ABSPATH . 'wp-content/uploads/summary-uploads/')) {
        mkdir(ABSPATH . 'wp-content/uploads/summary-uploads/', 0777, true);
    }
}

function summary_init_default_settings()
{
    global $unit_of_work;

    $allowed_formats = array(
        'pdf',
        'txt',
        'doc',
        'docx',
        'odp',
        'ods',
        'odt',
        'ppt',
        'pptx',
        'rar',
        'xls',
        'xlsx',
        'zip',
        '7z'
    );

    $unit_of_work->general_repository->set_summary_message('Faile lingina mitte lisada! Tõmba vajalikud failid alla ja lae need siia üles.');
    $unit_of_work->general_repository->set_allowed_file_formats($allowed_formats);
    $unit_of_work->general_repository->set_max_upload_file_size(10485760);
    $unit_of_work->general_repository->set_summary_min_length(10);
    $unit_of_work->general_repository->set_summary_max_length(10000);
}

function summary_init_create_pages()
{
    summary_init_create_post('projects', 'Projektid', 'page', summary_projects_list());
    summary_init_create_post('project-summary', '#project_summary#', 'page', summary_summary_form());
    summary_init_create_post('project-summary-view', '#project_summary_view#', 'page', summary_summary_form_view());
}

function summary_init_create_post(string $post_name, string $post_title, string $post_type, string $post_content): int
{
    global $wpdb;

    if (null === $wpdb->get_row("SELECT post_name FROM  {$wpdb->posts} WHERE post_name = '$post_name'", 'ARRAY_A')) {

        // create post object
        $post = array(
            'post_title' => $post_title,
            'post_status' => 'publish',
            'post_author' => wp_get_current_user()->ID,
            'post_type' => $post_type,
            'post_content' => $post_content,
            'post_name' => $post_name
        );

        // insert the post into the database
        return wp_insert_post($post);
    }

    return 0;
}

summary_init_create_file_structure();
summary_init_create_pages();
summary_init_default_settings();
