<?php

if (!defined('FILE_POST_TYPE')) {
    define('FILE_POST_TYPE', 'file');
}

if (!defined('POSITION_POST_TYPE')) {
    define('POSITION_POST_TYPE', 'position');
}

if (!defined('PROJECT_NAME_POST_TYPE')) {
    define('PROJECT_NAME_POST_TYPE', 'project_name');
}

if (!defined('PROJECT_POST_TYPE')) {
    define('PROJECT_POST_TYPE', 'project');
}

if (!defined('SUMMARY_POST_TYPE')) {
    define('SUMMARY_POST_TYPE', 'summary');
}

if (!defined('LOG_POST_TYPE')) {
    define('LOG_POST_TYPE', 'log');
}