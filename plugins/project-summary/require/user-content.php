<?php

//This file will contain all shortcode registrations.
//Templates and views should be created in separate files.

function summary_older_projects_list(): bool|string
{
    ob_start();
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/user-content/older-projects-list.php';
    return ob_get_clean();
}

add_shortcode('OLDER_PROJECTS_LIST', 'summary_older_projects_list');

function summary_user_projects_list(): bool|string
{
    ob_start();
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/user-content/positions-list.php';
    return ob_get_clean();
}

add_shortcode('USER_PROJECTS_LIST', 'summary_user_projects_list');

function summary_user_position_summaries_form(): bool|string
{
    ob_start();
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/user-content/summaries-form.php';
    return ob_get_clean();
}

add_shortcode('SUMMARIES_FORM', 'summary_user_position_summaries_form');

function summary_user_position_summaries_form_view(): bool|string
{
    ob_start();
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/user-content/summaries-view-form.php';
    return ob_get_clean();
}

add_shortcode('SUMMARIES_FORM_VIEW', 'summary_user_position_summaries_form_view');