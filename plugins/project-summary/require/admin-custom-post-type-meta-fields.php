<?php

function summary_title_field_placeholder($title, $post)
{
    if ($post->post_type === PROJECT_NAME_POST_TYPE) {
        $title = 'Enter project name here';
    }
    if ($post->post_type === POSITION_POST_TYPE) {
        $title = 'Enter position name here';
    }
    return $title;
}

add_filter('enter_title_here', 'summary_title_field_placeholder', 10, 2);

function summary_add_meta_boxes()
{
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/admin/partial-templates/position-edit.php';
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/admin/partial-templates/project-edit.php';
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/admin/partial-templates/summary-edit.php';
    require_once SUMMARY_ABS_PATH_PLUGIN_ROOT . 'templates/admin/partial-templates/file-edit.php';
}

add_action('add_meta_boxes', 'summary_add_meta_boxes');

function summary_update_meta(int $post_id)
{
    global $unit_of_work;

    if (isset($_FILES['admin_file_upload']) && isset($_POST['admin_file_position']) && is_numeric($_POST['admin_file_position'])) {
        $msg = $unit_of_work->file_repository->update_upload('admin_file_upload', $post_id, intval($_POST['admin_file_position']));
        if ($msg !== true) summary_admin_notify($msg);
    }

    if (isset($_POST['admin_summary_content'])) {
        $unit_of_work->summary_repository->update_content($post_id, sanitize_textarea_field($_POST['admin_summary_content']));
    }

    if (isset($_POST['admin_summary_clarification'])) {
        $unit_of_work->summary_repository->update_clarification($post_id, sanitize_text_field($_POST['admin_summary_clarification']));
    }

    if (isset($_POST['admin_position_project']) && is_numeric($_POST['admin_position_project'])) {
        $unit_of_work->position_repository->update_project($post_id, $_POST['admin_position_project']);
    }

    if (isset($_POST['admin_position_user']) && is_numeric($_POST['admin_position_user'])) {
        $unit_of_work->position_repository->update_user($post_id, $_POST['admin_position_user']);
    }

    if (isset($_POST['admin_position_reset_timer']) && $_POST['admin_position_reset_timer'] == 'on') {
        $unit_of_work->position_repository->reset_timer($post_id);
    }

    if (isset($_POST['admin_project_occurrence']) &&
        is_numeric($_POST['admin_project_occurrence']) &&
        isset($_POST['admin_project_occurrence_time'])
    ) {
        $unit_of_work->project_repository->update_meta(
            $post_id,
            intval($_POST['admin_project_occurrence']),
            sanitize_text_field($_POST['admin_project_occurrence_time'])
        );
    }

    if (isset($_POST['admin_summary_position']) && is_numeric($_POST['admin_summary_position'])) {
        $unit_of_work->summary_repository->update_position($post_id, $_POST['admin_summary_position']);
    }

    if (isset($_POST['admin_position_copy_select']) && is_numeric($_POST['admin_position_copy_select'])) {
        $unit_of_work->position_repository->copy($post_id, $_POST['admin_position_copy_select']);
    }

    if (isset($_POST['admin_project_copy_select']) && is_numeric($_POST['admin_project_copy_select'])) {
        $unit_of_work->project_repository->copy($post_id, intval($_POST['admin_project_copy_select']));
    }

    if (isset($_POST['admin_position_meta'])) {
        $unit_of_work->position_repository->update_meta($post_id, sanitize_textarea_field($_POST['admin_position_meta']));
    }

    if (isset($_POST['admin_position_meta_from_summary'])) {
        $position = $unit_of_work->position_repository->get_first_by_summary($post_id);
        if ($position) {
            $unit_of_work->position_repository->update_meta($position->id, sanitize_textarea_field($_POST['admin_position_meta']));
        }
    }


}

add_action('save_post_no_loop', 'summary_update_meta');

function summary_admin_edit_load_scripts()
{
    global $post_type;

    switch ($post_type) {
        case PROJECT_NAME_POST_TYPE:
            wp_enqueue_script('summary_project_name_creation_validation');
            wp_enqueue_style('summary_project_creation_validation_styles');
            break;
        case PROJECT_POST_TYPE:
            wp_enqueue_script('summary_project_creation_validation');
            wp_enqueue_style('summary_project_creation_validation_styles');
            break;
        case POSITION_POST_TYPE:
            wp_enqueue_script('summary_position_creation_validation');
            wp_enqueue_style('summary_project_creation_validation_styles');
            break;
        case SUMMARY_POST_TYPE:
            wp_enqueue_script('summary_summary_creation_validation');
            wp_enqueue_style('summary_project_creation_validation_styles');
            break;
        case FILE_POST_TYPE:
            wp_enqueue_script('summary_file_creation_validation');
            wp_enqueue_style('summary_project_creation_validation_styles');
            break;
        default:
            break;
    }
}

add_action('admin_print_scripts-post-new.php', 'summary_admin_edit_load_scripts');
add_action('admin_print_scripts-post.php', 'summary_admin_edit_load_scripts');

