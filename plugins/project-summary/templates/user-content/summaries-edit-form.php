<?php

import_class(Meta_Item::class);

global $unit_of_work;

if (!isset($_GET['position']) || intval($_GET['position'] == 0)) {
    echo 'No content to show!';
    return;
}

if (isset($_GET['item'])) {
    $current_item_id = $_GET['item'];
} else {
    $first = $unit_of_work->summary_repository->first_by_position($_GET['position']);
    if ($first != null) {
        $current_item_id = $first->id;
    } else {
        $current_item_id = 0;
        echo 'No content to show!';
        return;
    }
}

$summaries = $unit_of_work->summary_repository->get_all_by_position($_GET['position']);

if (!$summaries || !$unit_of_work->summary_repository->exists(intval($current_item_id)) && $current_item_id != 'file') {
    echo 'No content to show!';
    return;
}
$position = $unit_of_work->position_repository->get_first($_GET['position']);

require_once 'partial-templates/summaries-form.php';

$summary_min_len = $unit_of_work->general_repository->get_summary_min_length();
$summary_max_len = $unit_of_work->general_repository->get_summary_max_length();

wp_enqueue_style('summary_content');
wp_enqueue_style('markdown_style');
wp_enqueue_script('markdown_editor');
wp_enqueue_script('markdown_init');
wp_enqueue_script('summary_content_edit');

?>
    <div class="row">
        <div class="col-md-4">
            <?php
            foreach ($summaries as $i => $row) {
                summary_the_edit_list_item($_GET['position'], $row->id, $row->heading, array(), $current_item_id == $row->id);
            }

            summary_the_edit_list_item($_GET['position'], 'file', 'Failid', array('fw-bold'), $current_item_id == 'file');
            ?>
        </div>
        <div class='col-md-8'>
            <?php
            summary_the_message($unit_of_work->general_repository->get_summary_message());

            $meta = new Meta_Item('references', 'Viited ja muud märkmed', $position->meta);

            summary_the_summary_editor($current_item_id, $meta);
            ?>
        </div>
    </div>
