<?php

global $unit_of_work;

$current_user_id = $unit_of_work->user_repository->get_current_user_id();

if (!$unit_of_work->position_repository->user_can_view(intval($_GET['position']), $current_user_id))
    exit();

require_once 'partial-templates/summaries-form.php';

$summaries = $unit_of_work->summary_repository->get_all_by_position(intval($_GET['position']));
if (!$summaries) {
    echo 'No content to show!';
    return;
}

$position = $unit_of_work->position_meta_repository->get_first(intval($_GET['position']));

wp_enqueue_style('summary_content');

$previous_position = $unit_of_work->position_meta_repository->get_previous(intval($_GET['position']));
$next_position = $unit_of_work->position_meta_repository->get_next(intval($_GET['position']));
$previous_link = $previous_position != null ? summary_get_summary_view_link($previous_position->id) : '#';
$next_link = $next_position != null ? summary_get_summary_view_link($next_position->id) : '#';

?>
<div class="row text-center mt-3 mb-5">
    <div class="col-md-2 col-lg-1 link-item link-item-hover d-none d-md-inline <?= $previous_position == null ? 'link-item-disabled' : '' ?>" <?= $previous_position == null ? '' : "onclick='location.href=\"$previous_link\"';" ?>>
        <?php if ($previous_position): ?>
            <div class="row">
                <i class="h1 bi bi-arrow-left-circle"></i>
            </div>
            <div class="row fw-bold">
                <?= $previous_position->project_display_name ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-12 col-md-8 col-lg-10 text-center">
        <h1 class="display-3"><?= $position->name ?> - <?= $position->project_display_name ?></h1>
        <p class="h3"><?= $position->user_display_name ?></p>
    </div>
    <div class="col-md-2 col-lg-1 link-item link-item-hover d-none d-md-inline <?= $next_position == null ? 'link-item-disabled' : '' ?>" <?= $next_position == null ? '' : "onclick='location.href=\"$next_link\"';" ?>>
        <?php if ($next_position): ?>
            <div class="row">
                <i class="h1 bi bi-arrow-right-circle"></i>
            </div>
            <div class="row fw-bold">
                <?= $next_position->project_display_name ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php if ($previous_position || $next_position): ?>
    <div class="row d-md-none">
        <div class="d-grid gap-2 col-6">
            <?php if ($previous_position): ?>
                <a href="<?= $previous_link ?>" class="btn btn-outline-secondary" type="button">
                    <i class="bi bi-arrow-left-circle"></i>
                    <?= $previous_position->project_display_name ?>
                </a>
            <?php else: ?>
                <span class="btn btn-outline-secondary disabled">
                    No previous project!
                </span>
            <?php endif; ?>
        </div>
        <div class="d-grid gap-2 col-6">
            <?php if ($next_position): ?>
                <a href="<?= $next_link ?>" class="btn btn-outline-secondary" type="button">
                    <?= $next_position->project_display_name ?>
                    <i class="bi bi-arrow-right-circle"></i>
                </a>
            <?php else: ?>
                <span class="btn btn-outline-secondary disabled">
                    No next project!
                </span>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-md-4 my-3">
        <?php
        foreach ($summaries as $i => $row) {
            summary_the_list_item($row->id, $row->heading, array(), $i == 0);
        }
        $meta = $unit_of_work->position_repository->get_first($_GET['position']);
        $meta_items = array(
            new Meta_Item('meta', 'Viited ja muud märkmed', $meta->meta),
        );

        foreach ($meta_items as $item) {
            summary_the_list_item($item->id, $item->heading, array('fw-bold'));
        }
        summary_the_list_item('file', 'Failid', array('fw-bold'));
        ?>
    </div>
    <div class='col-md-8'>
        <?php
        summary_the_summary_view($summaries, $meta_items);
        ?>
    </div>
</div>