<?php
global $unit_of_work;
wp_enqueue_style('summary_list_page');
?>
    <ul class="list-group">
        <?php
        $visible_project_names = $unit_of_work->project_name_repository->get_all_visible(get_current_user_id());
        foreach ($visible_project_names as $name): ?>
            <a data-bs-toggle="collapse"
               id="parent-<?= $name->id ?>"
               href="#name-<?= $name->id ?>"
               role="button"
               aria-expanded="false"
               class="list-group-item list-group-item-action d-flex justify-content-between align-items-start grand-parent"
            >
                <div class="ms-2 me-auto">
                    <div class="fw-bold">
                        <?= $name->name ?>
                    </div>
                </div>
            </a>
            <?php
            summary_the_project_name_sub_items($name->id, $name->projects);
        endforeach;
        if (sizeof($visible_project_names) == 0): ?>
            <div class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">
                        Sul ei ole ligipääsu ühelegi projektile!
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </ul>

<?php

/**
 * @param int $project_name_id
 * @param array<Project_Old> $projects
 */
function summary_the_project_name_sub_items(int $project_name_id, array $projects)
{
    ?>
    <div class="collapse" id="name-<?= $project_name_id ?>">
        <ul class="list-group">
            <?php
            foreach ($projects as $project):?>
                <a href="#project-<?= $project->id ?>"
                   data-bs-toggle="collapse"
                   role="button"
                   aria-expanded="false"
                   class="mx-3 list-group-item list-group-item-action d-flex justify-content-between align-items-start parent"
                >
                    <div class="ms-2 me-auto">
                        <div class="fw-bold">
                            <?= $project->name ?>
                        </div>
                        <?php

                        if ($project->time_to_hide > 0) {
                            $time = time() + $project->time_to_hide;
                            echo '<span class="text-danger">Õigus näha kaob pärast: ' . summary_date("d M Y H:i:s", $time) . '</span>';
                        }
                        ?>
                    </div>
                </a>
                <?php
                summary_the_projects_sub_items($project->id, $project->positions);
            endforeach;
            if (sizeof($projects) == 0): ?>
                <div class="mx-3 list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        <div class="fw-bold">
                            Ei leidnud ühtegi projekti!
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </ul>
    </div>
    <?php
}

/**
 * @param int $project_id
 * @param array<Position> $positions
 */
function summary_the_projects_sub_items(int $project_id, array $positions)
{
    global $unit_of_work;

    ?>
    <div class="collapse" id="project-<?= $project_id ?>">
        <ul class="list-group">
            <?php
            $summary_list_link = get_permalink(get_page_by_title('#project_summary_view#'));

            foreach ($positions as $position):

                ?>
                <a href="<?= summary_add_param_to_url($summary_list_link, 'position', $position->id) ?>"
                   class="mx-5 list-group-item list-group-item-action d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        <div class="fw-bold"><?= $position->name ?></div>

                        <?php
                        $user = $unit_of_work->user_repository->get_first_by_position($position->id);
                        if ($user) {
                            echo $user->display_name;
                        } else {
                            echo '<div class="no-user">Ei leidnud kasutajat!</div>';
                        }
                        ?>
                    </div>
                </a>
            <?php endforeach;
            if (sizeof($positions) == 0): ?>
                <div class="mx-3 list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        <div class="fw-bold">
                            Ei leidnud ühtegi positsiooni!
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </ul>
    </div>
    <?php
}
