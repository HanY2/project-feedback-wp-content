<?php

function summary_projects_list(): string
{
    return "
<div class='row mt-3'>
    <div class='col-lg-5 mt-3'>
        <h2>Minu projektid</h2>
        [USER_PROJECTS_LIST]
    </div>
    <div class='col-lg-2'></div>
    <div class='col-lg-5 mt-3'>
        <h2>Varasemad projektid</h2>
        [OLDER_PROJECTS_LIST]
    </div>
</div>
";
}

