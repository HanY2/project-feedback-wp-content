<?php
global $unit_of_work;

$data = $unit_of_work->position_meta_repository->get_all_by_user(get_current_user_id(), false);
?>

<ul class="list-group">
    <?php foreach ($data as $row): ?>
        <a href="<?= summary_get_summary_edit_link($row->id) ?>"
           class="list-group-item list-group-item-action d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">
                <div class="fw-bold">
                    <?= $row->name ?>
                </div>
                <?= $row->project_display_name ?>
                <?php
                $time_to_hide = $unit_of_work->position_repository->time_for_position_hiding_remaining($row->id);

                if ($time_to_hide != 0) {
                    $time = time() + $time_to_hide;
                    echo '<div class="text-danger">Muutuseid saad teha kuni: ' . summary_date("d M Y H:i:s", $time) . '</div>';
                }
                ?>
            </div>
        </a>

    <?php endforeach;
    if (sizeof($data) == 0): ?>
        <div class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">
                <div class="fw-bold">
                    Ei leidnud ühtegi projekti, millele pead kokkuvõtet koostama!
                </div>
            </div>
        </div>
    <?php endif; ?>
</ul>
