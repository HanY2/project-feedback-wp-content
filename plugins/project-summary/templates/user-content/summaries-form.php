<?php

global $unit_of_work;

if (!isset($_GET['position']) || intval($_GET['position'] == 0))
    exit();

if (!$unit_of_work->position_repository->user_can_edit(intval($_GET['position']), get_current_user_id())) {
    get_template_part(403);
    exit();
}

$position = $unit_of_work->position_meta_repository->get_first($_GET['position']);

?>

    <div class="row mt-3 mb-5">
        <div class="col-12 text-center">
            <h1 class="display-3"><?= $position->name ?> - <?= $position->project_display_name ?></h1>
        </div>
    </div>

<?php

if ($unit_of_work->position_repository->time_for_position_hiding_remaining(intval($_GET['position'])) >= 0) {
    require_once 'summaries-edit-form.php';
} else {
    require_once 'summaries-edit-form-locked.php';
}