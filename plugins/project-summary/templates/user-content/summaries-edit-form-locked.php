<?php

global $unit_of_work;

$summaries = $unit_of_work->summary_repository->get_all_by_position(intval($_GET['position']));

if (!$summaries) {
    echo 'No content to show!';
    return;
}

require_once 'partial-templates/summaries-form.php';

wp_enqueue_style('summary_content');

?>
<div class="row">
    <div class="col-md-4 my-3">
        <?php
        foreach ($summaries as $i => $row) {
            summary_the_list_item($row->id, $row->heading, array(), $i == 0);
        }
        $meta = $unit_of_work->position_repository->get_first($_GET['position']);
        $meta_items = array(
            new Meta_Item('meta', 'Viited ja muud märkmed', $meta->meta),
        );

        foreach ($meta_items as $item) {
            summary_the_list_item($item->id, $item->heading, array('fw-bold'));
        }
        summary_the_list_item('file', 'Lisatud failid', array('fw-bold'));
        ?>
    </div>
    <div class='col-md-8'>
        <?php
        summary_the_message("You can not edit this summary!");
        summary_the_summary_view($summaries, $meta_items);
        ?>
    </div>
</div>