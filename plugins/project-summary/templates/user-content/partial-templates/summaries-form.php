<?php

import_class(Misc_Util::class);
import_class(Update_Result::class);
import_class(Summary::class);
import_class(Meta_Item::class);

function summary_the_edit_list_item($position_id, $id, $heading, array $classes = array(), bool $active = false)
{
    $url = summary_add_param_to_url(summary_get_current_url(), 'position', $position_id);
    $url = summary_add_param_to_url($url, 'item', $id);

    ?>
    <a href="<?= $url ?>"
       aria-expanded="<?= $active ? 'true' : 'false' ?>"
       class="list-group-item list-group-item-action d-flex justify-content-between align-items-start custom-aria-expanded <?= implode(' ', $classes) ?>">
        <div class="ms-2 me-auto">
            <?= $heading ?>
        </div>
    </a>
    <?php
}

function summary_the_list_item($id, $heading, array $classes = array(), bool $active = false)
{
    ?>
    <a href="#item-<?= $id ?>"
       role="button"
       aria-expanded="<?= $active ? 'true' : 'false' ?>"
       data-bs-toggle="collapse"
       class="list-group-item list-group-item-action d-flex justify-content-between align-items-start custom-aria-expanded <?= implode(' ', $classes) ?>">
        <div class="ms-2 me-auto">
            <?= $heading ?>
        </div>
    </a>
    <?php
}

/**
 * @param array<Summary> $summaries
 * @param array<Meta_Item> $meta_items
 */
function summary_the_summary_view(array $summaries, array $meta_items)
{
    //TODO meta content
    $md_parser = new Parsedown();
    $md_parser->setBreaksEnabled(true);
    global $unit_of_work;
    ?>
    <div id="content_parent">
        <?php foreach ($summaries as $i => $summary): ?>
            <div id="item-<?= $summary->id ?>" class="my-3 collapse <?= $i == 0 ? 'show' : '' ?>"
                 data-bs-parent="#content_parent">
                <?php summary_the_md_content_view($summary, $md_parser); ?>
            </div>
        <?php
        endforeach;
        foreach ($meta_items as $meta_item): ?>
            <div id="item-<?= $meta_item->id ?>" class="my-3 collapse"
                 data-bs-parent="#content_parent">
                <?php summary_the_plain_content_view($meta_item); ?>
            </div>
        <?php endforeach; ?>
        <div id="item-file" class='my-3 collapse' data-bs-parent="#content_parent">
            <?php $files = $unit_of_work->file_repository->get_all_by_position($_GET['position']);
            summary_the_viewable_files_list($files); ?>
        </div>
    </div>
    <?php
}

function summary_the_plain_content_view(Meta_Item $meta)
{
    ?>
    <h4 class="heading-border pb-2"><?= $meta->heading ?></h4>
    <span class="text-justify"><?= str_replace(PHP_EOL, '<br>', $meta->content) ?></span>
    <?php
}

function summary_the_md_content_view(Summary $summary, Parsedown $parser)
{
    ?>
    <h4 class="heading-border pb-2"><?= $summary->heading ?></h4>
    <span class="text-justify markdown-text"><?= $parser->text($summary->content); ?></span>
    <?php
}

/**
 * @param string $item_id
 * @param Meta_Item $reference_meta_item
 */
function summary_the_summary_editor(string $item_id, Meta_Item $reference_meta_item)
{
    global $unit_of_work;

    ?>
    <form method='POST' class="mt-3" action="<?= admin_url('admin-post.php') ?>" id='summary_summary'
          enctype="multipart/form-data">
        <div id="form_parent">
            <?php
            summary_the_errors();

            if ($item_id == 'file') {
                summary_the_file_editor(
                    $unit_of_work->file_repository->get_all_by_position($_GET['position']),
                    $unit_of_work->general_repository->get_max_upload_file_size(),
                    $unit_of_work->general_repository->get_allowed_file_formats()
                );
            } else if (is_numeric($item_id)) {
                $summary = $unit_of_work->summary_repository->get_first(intval($item_id));
                summary_the_content_editor($summary);
            }

            summary_the_meta_editor($reference_meta_item);

            summary_the_debug_checkbox($unit_of_work->user_repository->current_is_admin());
            ?>
        </div>
        <input type="hidden" id="item" name="item" value="<?= $item_id ?>">
        <input type="hidden" id='position' name="position" value="<?= $_GET['position'] ?>">
        <?= wp_nonce_field('summary_summary', 'summary_form_nonce') ?>
        <input type="hidden" id='action' name="action" value="summary_summary">

        <button type="submit" class="btn btn-primary">Uuenda</button>
    </form>
    <?php
}

function summary_the_meta_editor(Meta_Item $item)
{
    ?>
    <div id="item-<?= $item->id ?>" class='my-3' data-bs-parent="#form_parent">
        <label class="form-label" for="meta-item-editor-<?= $item->id ?>"><?= $item->heading ?></label>
        <textarea class="form-control" id="meta-item-editor-<?= $item->id ?>"
                  name="meta-item-<?= $item->id ?>"><?= $item->content ?></textarea>
    </div>

    <?php
}

function summary_the_file_editor(array $files, int $max_bytes, array $allowed_file_formats)
{
    summary_the_file_errors(); ?>
    <div class="row">
        <div class="col-6">
            <h5>Faili maksimaalne suurus:</h5>
            <p><?= Misc_Util::format_bytes($max_bytes) ?></p>
        </div>
        <div class="col-6">
            <h5>Lubatud formaadid:</h5>
            <p><?= implode(', ', $allowed_file_formats) ?></p>
        </div>
    </div>
    <?php summary_the_editable_files_list($files); ?>
    <label for='file_upload' class='form-label'>Lisa fail</label>
    <input class='form-control' type='file' name='file_upload' id='file_upload'>

    <?php
}

function summary_the_content_editor(Summary $summary)
{
    summary_the_md_textarea($summary->clarification, "content", $summary->content);
}

function summary_the_errors()
{
    if (isset($_GET['errors'])):
        foreach ($_GET['errors'] as $error): ?>
            <div class=" col alert alert-danger alert-dismissible fade show" role="alert">
                <?= $error ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert"
                        aria-label="Close"></button>
            </div>
        <?php endforeach;
    elseif (isset($_GET['status']) && $_GET['status'] == 1): ?>

        <div class=" col alert alert-success alert-dismissible fade show" role="alert">
            Uuendatud!
            <button type="button" class="btn-close" data-bs-dismiss="alert"
                    aria-label="Close"></button>
        </div>

    <?php endif;
}

function summary_the_file_errors()
{
    if (isset($_GET['file']['errors'])):
        foreach ($_GET['file']['errors'] as $error): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?= $error ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endforeach;
    endif;
}

/**
 * @param array<File_Entity> $files
 */
function summary_the_viewable_files_list(array $files)
{
    ?>
    <h5>Lisatud failid:</h5>
    <table class="table">
        <thead>
        <tr>
            <th style="min-width: 200px">Faili nimi</th>
            <th class="fit">Faili suurus</th>
            <th class="fit"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($files as $file): ?>
            <tr>
                <td>
                    <?= Misc_Util::to_readable_filename($file->title) ?>
                </td>
                <td class="fit"><?= Misc_Util::format_bytes($file->size) ?></td>
                <td class="fit">
                    <?= summary_the_file_download_link(
                        '<i class="bi bi-download" style="color: black"></i>',
                        'mx-2',
                        $file->id,
                        true
                    ) ?>
                </td>
            </tr>
        <?php endforeach;
        if (sizeof($files) == 0):?>
            <tr>
                <td>Ühtegi faili pole lisatud</td>
                <td></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <?php
}

/**
 * @param array<File_Entity> $files
 */
function summary_the_editable_files_list(array $files)
{
    ?>
    <h5>Üles laetud failid:</h5>
    <table class="table">
        <thead>
        <tr>
            <th class="fit">Kustuta</th>
            <th style="min-width: 200px">Faili nimi</th>
            <th class="fit">Faili maht</th>
            <th class="fit"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($files as $file): ?>
            <tr>
                <td class="fit">
                    <div class="form-check">
                        <input name="remove-<?= $file->id ?>"
                               class="form-check-input" type="checkbox"
                               id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                        </label>
                    </div>
                </td>
                <td>
                    <?= Misc_Util::to_readable_filename($file->title) ?>
                </td>
                <td class="fit"><?= Misc_Util::format_bytes($file->size) ?></td>
                <td class="fit">
                    <?= summary_the_file_download_link(
                        '<i class="bi bi-download" style="color: black"></i>',
                        'mx-2',
                        $file->id,
                        true
                    ) ?>
                </td>
            </tr>
        <?php endforeach;
        if (sizeof($files) == 0): ?>
            <tr>
                <td></td>
                <td>Ühtegi faili pole lisatud</td>
                <td></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <?php
}

function summary_the_debug_checkbox(bool $can_show = false)
{
    if ($can_show): ?>
        <input name="debug"
               class="form-check-input" type="checkbox"
               id="flexCheckDefault">
        <label class="form-check-label" for="flexCheckDefault">Debug</label>
    <?php endif;
}

function summary_the_md_textarea(string $title, string $textarea_name, string $content, int $size = 8)
{
    ?>
    <label id="textareaHeading" for="content" class="form-label">
        <?= $title ?>
    </label>
    <textarea aria-required="true"
              class="form-control content-editor" id="content"
              name="<?= $textarea_name ?>"
              rows="<?= $size ?>"><?= $content ?></textarea>
    <?php
}

function summary_the_message(string $message)
{
    echo "<small class='text-muted'>$message</small>";
}