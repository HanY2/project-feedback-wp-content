<?php
wp_enqueue_script('summary_admin_main_menu');

?>

<div class="wrap">
    <h1>Plugin settings</h1>
    <?php settings_errors(); ?>
    <form method="post" action="options.php">
        <?php
        wp_enqueue_script('summary_format_bytes');
        settings_fields('summary-settings-group');
        do_settings_sections('summary_admin_menu');
        submit_button();
        ?>
    </form>
</div>
