<?php

function summary_project_name_list_meta_box_html()
{
    global $unit_of_work;
    $projects = $unit_of_work->project_name_repository->get_all();
    if ($projects) {
        ?>
        <label for="summary_project_name_selection"></label>
        <select name="admin_project_occurrence" id="summary_project_name_selection">
            <option> -- Please select! --</option>
            <?php
            $current = $unit_of_work->project_name_repository->get_first_by_project(get_the_ID());
            $current_id = $current ? $current->id : 0;
            foreach ($projects as $project): ?>
                <option value="<?= $project->id ?>" <?= selected($current_id, $project->id, false) ?> >
                    <?= $project->name ?>
                </option>
            <?php endforeach; ?>
        </select>

        <?php

    } else {
        //TODO insert link here
        echo '<p>Please create a project name to finish project creation properly!</p>';
    }
}

function summary_project_time_meta_box_html()
{
    global $unit_of_work;
    $time = $unit_of_work->project_repository->get_time(get_the_ID());

    ?>
    <p>If project with same time and name exists, project will be saved as a draft.</p>
    <label for="project_time"></label>
    <input type="text" id="project_time" name="admin_project_occurrence_time" placeholder="Enter project time here"
           value="<?= $time ?>"/>
    <?php
}

function summary_project_setup_copy()
{
    global $unit_of_work;
    $projects = $unit_of_work->project_repository->get_all();
    if ($unit_of_work->project_repository->has_positions(get_the_ID())) {
        echo '<p>This project already has positions set up!</p>';
    } else if ($projects) {
        ?>
        <p>If a project has been selected, application attempts to copy summary setup from selected project.
            (Usually works)</p>
        <p>All positions will be copied from selected project, including summaries <strong>without content and without
                selecting users</strong>.</p>
        <p>Copying is not allowed when current project already has positions set up!</p>
        <label for="summary_project_copy_select"></label>
        <select name="admin_project_copy_select" id="summary_project_copy_select">
            <option>--</option>
            <?php foreach ($projects as $project): ?>
                <?php if ($project->id == get_the_ID()) continue; ?>
                <option value="<?= $project->id ?>">
                    <?= $project->name ?>
                </option>
            <?php endforeach; ?>
        </select>

        <?php
    } else {
        //TODO insert link here
        echo '<p>Please create a project name to finish project creation properly!</p>';
    }
}

add_meta_box('summary_project_occurrence_name', 'Project Name', 'summary_project_name_list_meta_box_html', [PROJECT_POST_TYPE]);
add_meta_box('summary_project_occurrence_time', 'Project Time', 'summary_project_time_meta_box_html', [PROJECT_POST_TYPE]);
add_meta_box('summary_project_copy', 'Copy setup', 'summary_project_setup_copy', [PROJECT_POST_TYPE]);