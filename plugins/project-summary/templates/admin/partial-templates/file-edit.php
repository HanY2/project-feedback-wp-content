<?php

function summary_file_title_meta_box()
{
    global $unit_of_work;
    $filename = $unit_of_work->file_repository->get_filename(get_the_ID());

    if ($filename) {
        echo "<p>$filename</p>";
    } else {
        echo '<p>Unique name will be generated upon file creation</p>';
    }
}

function summary_file_position_selection_meta_box()
{
    global $unit_of_work;
    $positions = $unit_of_work->position_meta_repository->get_all();

    if ($positions) {
        ?>
        <label for="summary_file_position_select"></label>
        <select name="admin_file_position" id="summary_file_position_select">
            <option> -- Please select! --</option>
            <?php
            $current = $unit_of_work->position_repository->get_first_by_file(get_the_ID());
            $current_id = $current ? $current->id : 0;
            foreach ($positions as $position): ?>
                <option value="<?= $position->id ?>" <?= selected($current_id, $position->id, false) ?> >
                    <?= $position->name ?>, <?= $position->project_display_name ?> (<?= $position->user_display_name ?>)
                </option>
            <?php endforeach; ?>
        </select>

        <?php
    } else {
        //TODO insert link here
        echo '<p>Please create a position first to finish file creation properly!</p>';
    }
}

function summary_file_upload_meta_box()
{
    global $unit_of_work;
    $file_title = $unit_of_work->file_repository->get_title(get_the_ID());

    if ($file_title): ?>
        <p>
            <small>Once updated, old file will be deleted and a new one will be replaced by the uploaded file.</small>
        </p>
        <p id="old_file_check"><?= summary_the_file_download_link($file_title, '', get_the_ID(), true) ?></p>
    <?php endif; ?>

    <label for='admin_file_upload_element'>Test</label>
    <input type='file' name='admin_file_upload' id='admin_file_upload_element'>
    <?php
}


add_meta_box('summary_file_unique_name', 'Unique name', 'summary_file_title_meta_box', [FILE_POST_TYPE]);
add_meta_box('summary_file_position', 'Position', 'summary_file_position_selection_meta_box', [FILE_POST_TYPE]);
add_meta_box('summary_upload_file', 'Upload file', 'summary_file_upload_meta_box', [FILE_POST_TYPE]);
