<?php

import_class(Summary::class);

function summary_summary_position_selection_meta_box()
{
    global $unit_of_work;

    $current = $unit_of_work->position_repository->get_first_by_summary(get_the_ID());
    $current_id = $current ? $current->id : 0;

    $positions = $unit_of_work->position_meta_repository->get_all_where_timer_has_not_elapsed($current_id);

    ?>
    <p>You may not see some positions, because they have been completed and their visibility timer has elapsed.</p>
    <p>If you want to select a position that is not visible here, <strong>reset its visibility timer</strong>!
        This will let the position to be seen on the list.</p>

    <?php if (sizeof($positions)): ?>
        <label for="summary_summary_position_element"></label>
        <select name="admin_summary_position" id="summary_summary_position_element">
            <option> -- Please select! --</option>
            <?php foreach ($positions as $position): ?>
                <option value="<?= $position->id ?>" <?= selected($current_id, $position->id, false) ?> >
                    <?= $position->name ?>, <?= $position->project_display_name ?> (<?= $position->user_display_name ?>)
                </option>
            <?php endforeach; ?>
        </select>

    <?php else:
        echo "<p><strong>No positions to show!</strong></p>";
    endif;
}

function summary_summary_clarification_editor()
{
    global $unit_of_work;
    $summary = $unit_of_work->summary_repository->get_first(get_the_ID());
    ?>

    <label for="summary_summary_clarification_editor"></label>
    <input type="text" name="admin_summary_clarification" style="min-width: 100%"
           id="summary_summary_clarification_editor"
           value="<?= $summary ? $summary->clarification : '' ?>"/>
    <?php
}

function summary_summary_content_editor()
{
    global $unit_of_work;
    $summary = $unit_of_work->summary_repository->get_first(get_the_ID());
    ?>
    <p>Supports markdown syntax</p>
    <label for="summary_summary_content_editor"></label>
    <textarea name="admin_summary_content"
              style="min-width: 100%; min-height: 200px"
              placeholder="For the user to fill out!"
              id="summary_summary_content_editor"><?php if ($summary) {
            echo $summary->content;
        } ?></textarea>
    <?php
}

function summary_position_meta_editor_for_summary()
{
    global $unit_of_work;
    $position = $unit_of_work->position_repository->get_first_by_summary(get_the_ID());
    ?>
        <p>Here goes positions meta, for example links for references.</p>
    <label for="summary_position_meta_editor"></label>
    <textarea name="admin_position_meta_from_summary"
              style="min-width: 100%; min-height: 100px"
              placeholder="For the user to fill out!"
              id="summary_position_meta_editor"><?php if ($position) {
            echo $position->meta;
        } ?></textarea>
    <?php
}

add_meta_box('summary_summary_content', 'Summary Content', 'summary_summary_content_editor', [SUMMARY_POST_TYPE]);
add_meta_box('summary_summary_clarification', 'Summary Clarification', 'summary_summary_clarification_editor', [SUMMARY_POST_TYPE]);
add_meta_box('summary_position_meta_by_summary', 'Position Meta', 'summary_position_meta_editor_for_summary', [SUMMARY_POST_TYPE]);
add_meta_box('summary_summary_position_selection', 'Position', 'summary_summary_position_selection_meta_box', [SUMMARY_POST_TYPE]);
