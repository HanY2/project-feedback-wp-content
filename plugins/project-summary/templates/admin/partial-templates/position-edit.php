<?php

import_class(Summary::class);

function summary_position_project_meta_box_html()
{
    global $unit_of_work;
    $projects = $unit_of_work->project_repository->get_all();

    if ($projects) {
        ?>
        <label for="summary_position_project_element"></label>
        <select name="admin_position_project" id="summary_position_project_element">
            <option> -- Please select! --</option>
            <?php
            $selected = $unit_of_work->project_repository->get_first_by_position(get_the_ID());
            $selected_id = $selected ? $selected->id : 0;
            foreach ($projects as $project): ?>
                <option value="<?= $project->id ?>" <?= selected($selected_id, $project->id, false) ?> >
                    <?= $project->name ?>
                </option>
            <?php endforeach; ?>
        </select>

        <?php

    } else {
        //TODO insert link here
        echo '<p>Please create a project first to finish position creation properly!</p>';
    }
}

function summary_position_user_meta_box_html()
{
    global $unit_of_work;
    $users = $unit_of_work->user_repository->get_all();

    if ($users) {
        ?>
        <label for="summary_position_user_element"></label>
        <select name="admin_position_user" id="summary_position_user_element">
            <option> -- Please select! --</option>
            <?php
            $current_user = $unit_of_work->user_repository->get_first_by_position(get_the_ID());
            $current_id = $current_user ? $current_user->ID : 0;

            foreach ($users as $user): ?>
                <option value="<?= $user->ID ?>" <?= selected($current_id, $user->ID, false) ?> >
                    <?= $user->display_name ?>
                </option>
            <?php endforeach; ?>
        </select>

        <?php

    } else {
        //TODO insert link here
        echo '<p>Please create a user first to finish position creation properly!</p>';
    }
}

function summary_reset_finished_timer()
{
    global $unit_of_work;
    if ($unit_of_work->position_repository->exists(get_the_ID())) {
        ?>
        <label for="summary_position_finished_element"></label>
        <input type="checkbox" id="summary_position_finished_element" name="admin_position_reset_timer">
        <?php
    } else {
        echo '<p>Nothing to show here!</p>';
    }
}

function summary_position_copy_setup()
{
    global $unit_of_work;
    if ($unit_of_work->position_repository->has_summaries(get_the_ID())) {
        echo '<p>This position already has summaries set up!</p>';
    } else {
        $positions = $unit_of_work->position_meta_repository->get_all();
        if ($positions) {
            ?>

            <p>If a position has been selected, application attempts to copy summary setup from selected position.
                (Usually works)</p>
            <p>All summaries will be copied from selected position <strong>without content and without selecting
                    users</strong>.</p>
            <p>Copying is not allowed when current position already has summaries set up!</p>
            <label for="summary_position_copy_select"></label>
            <select name="admin_position_copy_select" id="summary_position_copy_select">
                <option>--</option>
                <?php foreach ($positions as $position): ?>
                    <?php if ($position->id == get_the_ID()) continue; ?>
                    <option value="<?= $position->id ?>">
                        <?= $position->name ?> (<?= $position->project_display_name ?>)
                    </option>
                <?php endforeach; ?>
            </select>

            <?php
        } else {
            //TODO insert link here
            echo '<p>No positions found to copy from!</p>';
        }
    }
}

function summary_position_meta_editor()
{
    global $unit_of_work;
    $position = $unit_of_work->position_repository->get_first(get_the_ID());
    ?>

    <label for="summary_position_meta_editor"></label>
    <textarea name="admin_position_meta"
              style="min-width: 100%; min-height: 100px"
              placeholder="For the user to fill out!"
              id="summary_position_meta_editor"><?php if ($position) {
            echo $position->meta;
        } ?></textarea>
    <?php
}

add_meta_box('summary_position_meta', 'Meta input', 'summary_position_meta_editor', [POSITION_POST_TYPE]);
add_meta_box('summary_position_project', 'Project', 'summary_position_project_meta_box_html', [POSITION_POST_TYPE]);
add_meta_box('summary_position_user', 'User', 'summary_position_user_meta_box_html', [POSITION_POST_TYPE]);
add_meta_box('summary_position_reset_closed_timer', 'Reset finished timer', 'summary_reset_finished_timer', [POSITION_POST_TYPE]);
add_meta_box('summary_position_copy_other_setup', 'Copy setup', 'summary_position_copy_setup', [POSITION_POST_TYPE]);
