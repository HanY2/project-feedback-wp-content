<?php

use JetBrains\PhpStorm\Pure;

class FilenameAlreadyUsedException extends RuntimeException
{
    #[Pure] public function __construct($message = "Filename is already used!", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}