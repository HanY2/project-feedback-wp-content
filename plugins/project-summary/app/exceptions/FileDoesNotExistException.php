<?php

use JetBrains\PhpStorm\Pure;

class FileDoesNotExistException extends RuntimeException
{
    #[Pure] public function __construct($message = 'File does not exist!', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}