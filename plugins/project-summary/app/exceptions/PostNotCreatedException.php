<?php

use JetBrains\PhpStorm\Pure;

class PostNotCreatedException extends RuntimeException
{
    #[Pure] public function __construct($message = "Post creation failed!", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}