<?php

use JetBrains\PhpStorm\Pure;

class FileCreationFailedException extends RuntimeException
{
    #[Pure] public function __construct($message = "File creation failed", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}