<?php

use JetBrains\PhpStorm\Pure;

class ExceededFilesizeLimitException extends RuntimeException
{
    #[Pure] public function __construct($message = 'Exceeded filesize limit!', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}