<?php

use JetBrains\PhpStorm\Pure;

class FileUploadFailedInvalidParamsException extends RuntimeException
{
#[Pure] public function __construct($message = 'File upload unsuccessful! Invalid parameters.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}