<?php

use JetBrains\PhpStorm\Pure;

class NoFileFoundException extends RuntimeException
{
    #[Pure] public function __construct($message = "No file was found!", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}