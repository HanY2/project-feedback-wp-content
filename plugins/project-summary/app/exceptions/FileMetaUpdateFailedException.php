<?php

use JetBrains\PhpStorm\Pure;

class FileMetaUpdateFailedException extends RuntimeException
{
    #[Pure] public function __construct($message = "File meta update failed!", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}