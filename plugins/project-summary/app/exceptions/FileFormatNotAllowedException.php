<?php

use JetBrains\PhpStorm\Pure;

class FileFormatNotAllowedException extends RuntimeException
{
    #[Pure] public function __construct($message = "File format not allowed", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}