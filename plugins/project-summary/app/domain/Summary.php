<?php


class Summary
{

    public function __construct(int $id, int $position_id, string $heading, string $content, string $clarification)
    {
        $this->id = $id;
        $this->position_id = $position_id;
        $this->heading = $heading;
        $this->content = $content;
        $this->clarification = $clarification;
    }

    public int $id;
    public int $position_id;
    public string $heading;
    public string $content;
    public string $clarification;
}