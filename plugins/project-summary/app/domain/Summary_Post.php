<?php


class Summary_Post
{

    public function __construct(int $id, int $user_id, int $position_id, string $content)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->content = $content;
        $this->position_id = $position_id;
    }

    public int $id;
    public int $user_id;
    public string $content;
    public int $position_id;
}