<?php

import_class(IComparable::class);

class Project_Name_Old implements IComparable
{
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->projects = array();
    }

    public int $id;
    public string $name;
    /**
     * @var array<Project>
     */
    public array $projects;

    public function equals(mixed $other): bool
    {
        if (get_class($other) != self::class) {
            return false;
        }

        return $this->id == $other->id;
    }
}