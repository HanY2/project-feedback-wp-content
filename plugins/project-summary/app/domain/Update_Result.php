<?php


class Update_Result
{
    public const FAILED = 0;
    public const SUCCESS = 1;

    public int $id;
    public array $errors;
    public int $status;

    public function __construct(int $id, array $errors = array(), $status = self::FAILED)
    {
        $this->id = $id;
        $this->errors = $errors;
        $this->status = $status;
    }

    public function set_status(int $status): Update_Result
    {
        $this->status = $status;
        return $this;
    }
}