<?php

import_class(IComparable::class);

class Position implements IComparable
{
    public function __construct(int $id, string $name, int $project_id, int $user_id, int $completed_at, string $meta)
    {
        $this->id = $id;
        $this->name = $name;
        $this->project_id = $project_id;
        $this->user_id = $user_id;
        $this->completed_at = $completed_at;
        $this->meta = $meta;
    }

    public int $id;
    public string $name;
    public int $project_id;
    public int $user_id;
    public int $completed_at;
    public string $meta;

    public function equals(mixed $other): bool
    {
        if (get_class($other) != self::class) {
            return false;
        }

        return $this->id == $other->id;
    }
}