<?php

import_class(IComparable::class);

class Project_Old implements IComparable
{

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->positions = array();
        $this->time_to_hide = 0;
    }

    public int $id;
    public string $name;
    /**
     * @var array<Position_Old>
     */
    public array $positions;
    public int $time_to_hide;

    public function equals(mixed $other): bool
    {
        if (get_class($other) != self::class) {
            return false;
        }

        return $this->id == $other->id;
    }
}