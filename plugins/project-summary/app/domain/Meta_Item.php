<?php

class Meta_Item
{
    public string $id;
    public string $heading;
    public string $content;

    /**
     * @param string $id
     * @param string $heading
     * @param string $content
     */
    public function __construct(string $id, string $heading, string $content)
    {
        $this->id = $id;
        $this->heading = $heading;
        $this->content = $content;
    }


}