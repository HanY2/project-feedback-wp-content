<?php
import_class(IComparable::class);

class Position_Old implements IComparable
{
    public function __construct(int $id, string $name, int $project_id, int $user_id, int $time_to_hide)
    {
        $this->id = $id;
        $this->name = $name;
        $this->project_id = $project_id;
        $this->user_id = $user_id;
        $this->time_to_hide = $time_to_hide;
        $this->summaries = array();
    }

    public int $id;
    public string $name;
    public int $project_id;
    public int $user_id;
    public int $time_to_hide;
    /**
     * @var array<Summary>
     */
    public array $summaries;

    public function equals(mixed $other): bool
    {
        if (get_class($other) != self::class) {
            return false;
        }

        return $this->id == $other->id;
    }
}