<?php

import_class(IComparable::class);

class Position_Meta implements IComparable
{
    public function __construct(int $id,
                                string $name,
                                int $project_id,
                                int $user_id,
                                string $user_display_name,
                                string $project_display_name,
                                int $completed_at)
    {
        $this->id = $id;
        $this->name = $name;
        $this->project_id = $project_id;
        $this->user_id = $user_id;
        $this->completed_at = $completed_at;
        $this->user_display_name = $user_display_name;
        $this->project_display_name = $project_display_name;
    }

    public int $id;
    public string $name;
    public int $project_id;
    public int $user_id;
    public string $user_display_name;
    public string $project_display_name;
    public int $completed_at;

    public function equals(mixed $other): bool
    {
        if (get_class($other) != self::class) {
            return false;
        }

        return $this->id == $other->id;
    }
}