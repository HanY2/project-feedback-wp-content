<?php

import_class(IComparable::class);

class File_Entity implements IComparable
{
    public int $id;
    public int $position_id;

    /**
     * @var string file nice name
     */
    public string $title;

    /**
     * @var string file actual name
     */
    public string $filename;
    public int $size;

    public function __construct(int $id, int $position_id, string $filename, string $unique_name, int $size)
    {
        $this->id = $id;
        $this->position_id = $position_id;
        $this->title = $filename;
        $this->filename = $unique_name;
        $this->size = $size;
    }

    public function equals(mixed $other): bool
    {
        if (get_class($other) != self::class) {
            return false;
        }

        return $this->id == $other->id;
    }
}