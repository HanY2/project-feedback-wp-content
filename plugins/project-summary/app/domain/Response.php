<?php


class Response
{
    public const FAILED = 0;
    public const UPDATED = 1;


    /**
     * @var array<Update_Result>
     */
    public array $results = [];

    /**
     * @var array<string>
     */
    public array $global_errors = [];


    /**
     * @param array<string> $global_errors
     */
    public function __construct(array $global_errors = array())
    {
        $this->global_errors = $global_errors;
    }
}