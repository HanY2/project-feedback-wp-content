<?php

use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

import_class(Base_Post_Repository::class);
import_class(File_Entity::class);
import_class(Misc_Util::class);
import_class(FileFormatNotAllowedException::class);
import_class(ExceededFilesizeLimitException::class);
import_class(FileCreationFailedException::class);
import_class(FileDoesNotExistException::class);
import_class(FileMetaUpdateFailedException::class);
import_class(FilenameAlreadyUsedException::class);
import_class(FileUploadFailedInvalidParamsException::class);
import_class(NoFileFoundException::class);
import_class(PostNotCreatedException::class);


class File_Repository extends Base_Post_Repository
{
    #[Pure] public function __construct(UoW $uoW)
    {
        parent::__construct(FILE_POST_TYPE, $uoW);
    }

    public function get_first(int $id): ?File_Entity
    {
        return $this->get_as_entity($id);
    }

    /**
     * @return array<File_Entity>
     */
    public function get_all(): array
    {
        return $this->get_all_as_entity();
    }

    public function update_size(int $file_id, int $size): bool
    {
        if (update_post_meta($file_id, 'summary_file_size', $size)) {
            return true;
        }
        return false;
    }

    /**
     * Expects $file_id to be id of file and that for duplicates are checked.
     * @param int $file_id
     * @param int $position_id
     */
    private function update_position(int $file_id, int $position_id)
    {
        update_post_meta($file_id, 'summary_file_position', $position_id);
    }

    public function create_post(string $file_ext, string $file_title, int $size, int $position_id, int $file_id = 0): string
    {
        if ($this->uow->position_repository->has_file_with_title($position_id, $file_title)) {
            throw new FilenameAlreadyUsedException('Filename already used for this position');
        }

        $unique_name = Misc_Util::generate_random_filename($file_ext);

        $this->save_file_record($unique_name, $file_title, $size, $position_id, $file_id);
        return $unique_name;
    }

    /**
     * @param array $files
     * @param int $user_id
     * @return true|string true if success, on failure error as string
     */
    public function remove_files(array $files, int $user_id): string|bool
    {
        foreach ($files as $file_id) {
            if (!$this->exists($file_id)) {
                return 'Faili kustutamine ei õnnestunud!';
            }

            if (!$this->user_can_edit($file_id, $user_id)) {
                return 'Sul puuduvad õigused!';
            }
        }

        foreach ($files as $file_id) {
            if (!wp_delete_post($file_id, true)) {
                return 'Faili kustutamine ei õnnestunud!';
            }
        }

        return true;
    }

    /**
     * Create post for given file params. A post with $unique_name as title is generated.
     * @param string $unique_name
     * @param string $pretty_name
     * @param int $filesize
     * @param int $position_id
     * @param int $file_id optional, if non-zero, existing file post is searched and its title is updated
     */
    public function save_file_record(string $unique_name, string $pretty_name, int $filesize, int $position_id, int $file_id = 0)
    {
        if ($file_id != 0) {
            if (!$this->exists($file_id)) {
                throw new FileDoesNotExistException();
            }

            $this->update_post_title($file_id, $unique_name);

        } else {
            $params = array(
                'post_title' => $unique_name,
                'post_status' => 'publish',
                'post_type' => $this->post_type,
                'post_content' => '',
                'post_name' => "$unique_name"
            );

            $file_id = wp_insert_post($params);
        }

        if (!$file_id) {
            throw new PostNotCreatedException();
        }
        $this->update_file_title($file_id, $pretty_name);
        $this->update_position($file_id, $position_id);
        $this->update_size($file_id, $filesize);
    }

    /**
     * @param int $position_id
     * @return array<File_Entity>
     */
    public function get_all_by_position(int $position_id): array
    {
        global $wpdb;
        $sql_prepare = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta
            WHERE meta_key = 'summary_file_position' AND meta_value = '%s';", $position_id);

        $rows = $wpdb->get_results($sql_prepare, ARRAY_A);
        $res = array();
        foreach ($rows as $row) {
            $file = $this->get_first($row['post_id']);
            if ($file) {
                $res[] = $file;
            }
        }

        return $res;
    }

    public function get_size(int $file_id): int
    {
        return intval(get_post_meta($file_id, 'summary_file_size', true));
    }

    public function user_can_edit(int $file_id, int $user_id): bool
    {
        $position = $this->uow->position_repository->get_first_by_file($file_id);
        if (!$position) {
            return false;
        }

        return $user_id == $position->user_id;
    }

    public function file_post_has_file(int $file_id): bool
    {
        if ($this->get_title($file_id)) {
            return true;
        }
        return false;
    }

    public function get_filename(int $file_id, bool $ignore_status = false): ?string
    {
        if (!$ignore_status && !$this->exists($file_id)) {
            return null;
        }

        $post = get_post($file_id);
        return $post->post_title;
    }

    public function get_title(int $file_id): ?string
    {
        $res = get_post_meta($file_id, 'summary_file_filename', true);
        if (!$res) {
            return null;
        }

        return sanitize_text_field($res);
    }

    public function remove_title(int $file_id)
    {
        delete_post_meta($file_id, 'summary_file_filename');
    }

    public function update_file_title(int $file_id, string $new_name): bool
    {
        if (update_post_meta($file_id, 'summary_file_filename', $new_name)) {
            return true;
        }
        return false;
    }

    /**
     * @param string $input_name
     * @param int $file_id
     * @param int $position_id
     * @return true|string
     */
    public function update_upload(string $input_name, int $file_id, int $position_id): bool|string
    {
        $this->uow->summary_repository->update_file($position_id, $file_id);

        if (!$this->file_post_has_file($file_id)) {
            $status = $this->upload($input_name, $position_id, $file_id);
            if ($status === null) {
                return 'File upload failed';
            }
            if ($status !== true) {
                $this->update_post_status($file_id, 'draft');
            }

            return $status;
        }

        $new_name = $_FILES[$input_name]['name'];

        //get the post title, to keep old name;
        $unique_name = $this->get_filename($file_id);
        $name = explode('.', $unique_name)[0];
        try {
            $ext = $this->get_file_ext_from_FILES($input_name);

            $this->delete($file_id);

            $filename = self::create($name, $ext, $input_name);
            $this->update_post_title($file_id, $filename);

            if (!$this->update_file_title($file_id, $new_name)) {
                throw new FileMetaUpdateFailedException('Filename update failed!');
            }

        } catch (FileCreationFailedException | FileMetaUpdateFailedException $e) {
            $this->update_post_status($file_id, 'draft');
            $this->remove_title($file_id);
            return $e->getMessage();

        } catch (RuntimeException $e) {
            return $e->getMessage();
        }

        return true;
    }

    /**
     * @param string $input_name
     * @param int $position_id
     * @param int $file_id
     * @return true|string|null returns true on success, otherwise error message, null when no upload took place
     */
    public function upload(string $input_name, int $position_id, int $file_id = 0): bool|string|null
    {
        $unique_name = '';

        try {
            if (!self::has_no_file_error($input_name)) {
                $data = $this->validate_FILES($input_name);

                $unique_name = $this->create_post($data['ext'], $data['title'], $data['size'], $position_id, $file_id);

                $name = explode('.', $unique_name)[0];
                $ext = $this->get_file_ext_from_FILES($input_name);
                $this->create($name, $ext, $input_name);

                return true;
            }
            return null;
        } catch (FileMetaUpdateFailedException $e) {
            if ($unique_name) {
                $post = get_page_by_title($unique_name);
                $this->update_post_status($post->ID, 'draft');
            }
            return $e->getMessage();
        } catch (RuntimeException $e) {
            return $e->getMessage();
        }
    }

    public function delete(string $file_id)
    {
        $filename = $this->get_filename($file_id, true);
        unlink(SUMMARY_FILE_UPLOAD_FOLDER . $filename);
    }

    private static function create(string $filename, string $ext, string $input_name): string
    {
        if (!move_uploaded_file($_FILES[$input_name]['tmp_name'], SUMMARY_FILE_UPLOAD_FOLDER . "$filename.$ext"))
            throw new FileCreationFailedException();

        return "$filename.$ext";
    }

    private static function has_no_file_error(string $input_name): bool
    {
        return
            isset($_FILES[$input_name]) &&
            isset($_FILES[$input_name]['error']) &&
            $_FILES[$input_name]['error'] == UPLOAD_ERR_NO_FILE;
    }

    /**
     * @param string $input_name the name of input where file was selected
     * @return array file data
     */
    #[ArrayShape(['ext' => "string", 'title' => "string", 'size' => "int"])]
    private function validate_FILES(string $input_name): array
    {
        $allowed_file_size = $this->uow->general_repository->get_max_upload_file_size();

        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($_FILES[$input_name]['error']) ||
            is_array($_FILES[$input_name]['error'])
        ) {
            throw new FileUploadFailedInvalidParamsException();
        }

        // Check $_FILES[$input_name]['error'] value.
        switch ($_FILES[$input_name]['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new NoFileFoundException('No file sent!');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new ExceededFilesizeLimitException();
            default:
                throw new RuntimeException('Unknown error!');
        }

        // You should also check filesize here.
        if ($_FILES[$input_name]['size'] > $allowed_file_size) {
            throw new ExceededFilesizeLimitException();
        }

        $ext = $this->get_file_ext_from_FILES($input_name);

        return array(
            'ext' => $ext,
            'title' => strval($_FILES[$input_name]['name']),
            'size' => intval($_FILES[$input_name]['size']),
        );
    }

    private function get_file_ext_from_FILES($input_name): string
    {
        /*

        For validating file type by mime

        $allowed_mimes = $this->uow->general_repository->get_allowed_file_formats_as_dict();

        // DO NOT TRUST $_FILES[$input_name]['mime'] VALUE !!
        // Check MIME Type by yourself.

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mime = $finfo->file($_FILES[$input_name]['tmp_name']);

        if (false === $ext = array_search($mime, $allowed_mimes, true)) {
            $parts = explode('.', $_FILES[$input_name]['name']);
            throw new FileFormatNotAllowedException('File format not allowed: ' . end($parts));
        }

        return strval($ext);

        */

        $allowed_formats = $this->uow->general_repository->get_allowed_file_formats();
        $parts = explode('.', $_FILES[$input_name]['name']);
        $ext = end($parts);
        if (!in_array($ext, $allowed_formats)) {
            throw new FileFormatNotAllowedException('File format not allowed: ' . end($parts));
        }

        return $ext;
    }

    protected function get_mapper(): Closure
    {
        return function (WP_Post $post) {
            $position = $this->uow->position_repository->get_first_by_file($post->ID);
            $position_id = $position != null ? $position->id : 0;

            $filename = $this->get_title($post->ID) ?? 'Filename not found!';
            $filesize = $this->get_size($post->ID);

            return new File_Entity($post->ID, $position_id, $filename, $post->post_title, $filesize);
        };
    }
}