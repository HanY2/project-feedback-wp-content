<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Entity_Repository::class);

class User_Repository extends Base_Entity_Repository
{
    #[Pure] public function __construct(UoW $uow)
    {
        parent::__construct($uow);
    }

    public function get_first(int $id): ?WP_User
    {
        $user = get_user_by('ID', $id);
        if ($user === false) {
            return null;
        }
        return $user;
    }

    /**
     * @return array<WP_User>
     */
    public function get_all(): array
    {
        return get_users();
    }

    public function get_first_by_position(int $position_id): ?WP_User
    {
        $user_id = $this->get_user_id_by_position($position_id);

        $user = $this->get_first($user_id);
        if ($user) {
            return $user;
        }
        return null;
    }

    public function get_current_user_id(): int
    {
        return get_current_user_id();
    }

    public function get_user_id_by_position(int $position_id): int
    {
        $res = get_post_meta($position_id, 'summary_position_user', true);
        if ($res) {
            return intval($res);
        }
        return 0;
    }

    public function has_positions(int $user_id): bool
    {
        global $wpdb;
        $sql_prepare = $wpdb->prepare("SELECT EXISTS (SELECT * FROM $wpdb->postmeta
            WHERE meta_key = 'summary_position_user' AND meta_value = '%s') AS Result;", $user_id);
        $res = $wpdb->get_results($sql_prepare, ARRAY_A);

        return $res[0]['Result'] == 1;
    }

    public function exists(int $id): bool
    {
        global $wpdb;
        $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->users WHERE ID = %d", $id));
        return $count == 1;
    }

    public function is_admin(int $user_id): bool
    {
        return user_can($user_id, 'administrator');
    }

    public function current_is_admin(): bool
    {
        return current_user_can('administrator');
    }
}