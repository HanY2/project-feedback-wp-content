<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Post_Repository::class);
import_class(Summary::class);
import_class(Update_Result::class);
import_class(Summary_Post::class);
import_class(Response::class);
import_class(Entity_Mapper::class);


class Summary_Repository extends Base_Post_Repository
{
    #[Pure] public function __construct(UoW $uoW)
    {
        parent::__construct(SUMMARY_POST_TYPE, $uoW);
    }

    public function get_first(int $id): ?Summary
    {
        return $this->get_as_entity($id);
    }

    public function first_by_position(int $position_id): ?Summary
    {
        if (!$this->uow->position_repository->exists($position_id)) {
            return null;
        }
        global $wpdb;
        $summaries_prepare = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta
            WHERE meta_key = 'summary_summary_position' AND  meta_value = '%s' LIMIT 1", $position_id);
        $res = $wpdb->get_results($summaries_prepare, ARRAY_A);

        if (sizeof($res) != 1) {
            return null;
        }
        $summary = $res[0];

        $mapper = function ($row) {
            $id = $row['post_id'];
            return $this->get_first(intval($id));
        };

        return Entity_Mapper::map($summary, $mapper);
    }

    public function get_all(): array
    {
        return $this->get_all_as_entity();
    }

    public function get_length(int $id): int
    {
        $summary = self::get_first($id);
        if ($summary) {
            return strlen($summary->content);
        }
        return -1;
    }

    /**
     * Checks, whether a user can edit given summary. Same logic applies here as on checking for position.
     * @param int $id
     * @param int $user_id
     * @return bool
     */
    public function user_can_edit(int $id, int $user_id): bool
    {
        $position = $this->uow->position_repository->get_first_by_summary($id);
        $position_id = $position != null ? $position->id : 0;

        return $this->uow->position_repository->user_can_edit($position_id, $user_id);
    }

    public function get_clarification(int $id): string
    {
        return sanitize_text_field(get_post_meta($id, 'summary_summary_clarification', true)) ?? '';
    }

    /**
     * @param int $position_id
     * @return array<Summary>
     */
    public function get_all_by_position(int $position_id): array
    {
        global $wpdb;
        $summaries_prepare = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta
            WHERE meta_key = 'summary_summary_position' AND  meta_value = '%s'", $position_id);

        $summaries = $wpdb->get_results($summaries_prepare, ARRAY_A);
        $mapper = function ($row) {
            $id = $row['post_id'];
            return $this->get_first(intval($id));
        };

        return Entity_Mapper::map_all($summaries, $mapper);
    }

    /**
     * @param int $summary_id
     * @return array<Summary>
     */
    public function get_all_by_summary(int $summary_id): array
    {
        $position = $this->uow->position_repository->get_first_by_summary($summary_id);
        if ($position == null) {
            return array();
        }

        return $this->get_all_by_position($position->id);
    }

    public function update_content(int $id, string $content)
    {
        parent::update_content($id, $content);
    }

    public function update_position(int $summary_id, int $position_id)
    {
        update_post_meta($summary_id, 'summary_summary_position', $position_id);
    }

    public function update_clarification(int $summary_id, string $clarification)
    {
        update_post_meta($summary_id, 'summary_summary_clarification', $clarification);
    }

    public function update_file(int $position_id, int $file_id)
    {
        update_post_meta($file_id, 'summary_file_position', $position_id);
    }

    public function update_single(Summary_Post $summary): Update_Result
    {
        if ($this->uow->position_repository->is_locked($summary->position_id)) {
            return new Update_Result($summary->id, array('Kokkuvõtet ei saa muuta!'));
        }

        $update_res = $this->update_summary($summary);

        $this->uow->position_repository->reset_timer($summary->position_id);

        return $update_res;
    }

    /**
     * @param array<Summary_Post> $summaries
     * @param string $references_meta
     * @return Response
     */
    public function update_all(array $summaries, string $references_meta): Response
    {
        if (sizeof($summaries) == 0) {
            return new Response(array('Ei tea mida uuendada!'));
        }

        $position_id = $summaries[0]->position_id;
        if ($this->uow->position_repository->is_locked($position_id)) {
            return new Response(array('Kokkuvõtteid ei saa muuta!'));
        }

        $res = new Response();
        $is_position_completed = true;
        $summary_content_min_len = $this->uow->general_repository->get_summary_min_length();


        foreach ($summaries as $summary) {
            $update_result = $this->update_summary($summary);

            if (sizeof($update_result->errors) > 0 ||
                strlen($summary->content) < $summary_content_min_len) {
                $is_position_completed = false;
            }

            $res->results[] = $update_result;
        }

        if ($is_position_completed) {
            $this->uow->position_repository->set_complete($position_id);
        }

        $this->uow->position_repository->update_meta($position_id, $references_meta);

        return $res;
    }


    private function update_summary(Summary_Post $summary): Update_Result
    {
        $res = new Update_Result($summary->id);


        if (!$this->exists($summary->id)) {
            $res->errors[] = "Kokkuvõtet ei ole olemas!";
            return $res;
        }

        if (!$this->user_can_edit($summary->id, $summary->user_id)) {
            $res->errors[] = "Kasutaja viga!";
            return $res;
        }

        $res->errors = $this->validate_summary_content($summary);

        //TODO update and create in base_repo
        $post = get_post($summary->id);
        if (!$post || $post->post_type != 'summary') {
            $res->errors[] = 'Kokkuvõtet ei leitud!';
            return $res;
        }

        $post->post_content = $summary->content;

        if (wp_update_post($post) == $summary->id) {
            $res->status = Update_Result::SUCCESS;
        }

        return $res;
    }

    /**
     * @param Summary_Post $summary_Post
     * @return array<string> errors
     */
    private function validate_summary_content(Summary_Post $summary_Post): array
    {
        $result = array();
        $min_length = $this->uow->general_repository->get_summary_min_length();
        $max_length = $this->uow->general_repository->get_summary_max_length();

        $length_validation = self::is_summary_content_length_valid($summary_Post->content, $min_length, $max_length);
        if ($length_validation) {
            $result[] = $length_validation;
        }

        return $result;
    }

    private static function is_summary_content_length_valid(string $content, int $min_length, int $max_length): string
    {
        if (strlen($content) > $max_length) {
            return "Kokkuvõtte maksimaalne pikkus on $max_length tähemärki!";
        }
        if (strlen($content) < $min_length) {
            return "Kokkuvõtte minimaalne pikkus on $min_length tähemärki!";
        }

        return '';
    }

    public function summary_is_valid(int|Summary $summary): bool
    {
        if (gettype($summary) == 'integer') {
            $summary = $this->get_first($summary);
        }

        if ($summary == null) {
            return false;
        }

        $min_length = $this->uow->general_repository->get_summary_min_length();
        $max_length = $this->uow->general_repository->get_summary_max_length();

        $err = self::is_summary_content_length_valid($summary->content, $min_length, $max_length);
        if (strlen($err) == 0) {
            return true;
        }

        return false;
    }

    protected function get_mapper(): Closure
    {
        return function (WP_Post $post) {
            $position = $this->uow->position_repository->get_first_by_summary($post->ID);
            $clarification = $this->get_clarification($post->ID);

            return new Summary(
                $post->ID,
                $position != null ? $position->id : 0,
                $post->post_title,
                stripslashes($post->post_content),
                $clarification);
        };
    }
}