<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Post_Repository::class);
import_class(Project::class);

class Project_Repository extends Base_Post_Repository
{
    #[Pure] public function __construct(UoW $uow)
    {
        parent::__construct(PROJECT_POST_TYPE, $uow);
    }

    public function has_positions(int $project_id): bool
    {
        global $wpdb;
        $sql_prepare = $wpdb->prepare("SELECT EXISTS (SELECT * FROM $wpdb->postmeta
            WHERE meta_key = 'summary_position_project' AND meta_value = '%s') AS Result;", $project_id);
        $res = $wpdb->get_results($sql_prepare, ARRAY_A);

        return $res[0]['Result'] == 1;
    }

    public function get_all_of_same_project_name_by_position(int $position_id): array
    {
        $project = $this->uow->project_repository->get_first_by_position($position_id);
        if ($project == null) {
            return array();
        }
        $project_name = $this->uow->project_name_repository->get_first_by_project($project->id);

        return $this->uow->project_repository->get_all_by_project_name($project_name->id);
    }

    /**
     * @param int $project_name_id
     * @return array<Project>
     */
    public function get_all_by_project_name(int $project_name_id): array
    {
        return $this->get_all_as_entity(array(
            'meta_key' => 'summary_project_occurrence_name_id',
            'meta_value' => $project_name_id,
            'meta_compare' => '=',
        ));
    }

    public function get_first_by_title(string $title): ?Project
    {
        $page = get_page_by_title($title, OBJECT, $this->post_type);
        if (!$page) {
            return null;
        }

        return $this->get_mapper()->call($page);
    }

    public function get_first(int $id): ?Project
    {
        return $this->get_as_entity($id);
    }

    public function get_time(int $id): string
    {
        return get_post_meta($id, 'summary_project_occurrence_time', true);
    }

    /**
     * Gets Project by position id.
     *
     * @param $position_id int position id to search by
     * @return null|Project
     */
    public function get_first_by_position(int $position_id): ?Project
    {
        $project_id = intval(get_post_meta($position_id, 'summary_position_project', true));
        return $this->get_as_entity($project_id);
    }

    /**
     * @param int $user_id
     * @return array<Project>
     */
    public function get_all_by_user(int $user_id): array
    {
        $project_ids = array();

        $positions = $this->uow->position_repository->get_all_by_user($user_id);
        foreach ($positions as $position) {
            if (in_array($position->project_id, $project_ids)) {
                continue;
            }
            $project_ids[] = $position->project_id;
        }

        if (empty($project_ids)) {
            $project_ids = array(-1);
        }

        $params = [
            'post__in' => $project_ids,
        ];

        return $this->get_all_as_entity($params);
    }

    /**
     * @return array<Project>
     */
    public function get_all(): array
    {
        return $this->get_all_as_entity();
    }

    public function copy(int $project_to, int $project_from)
    {
        $positions = $this
            ->uow
            ->position_repository
            ->get_all_by_project($project_from);

        //TODO logic to base repo
        foreach ($positions as $position) {
            $position_post_data = array(
                'post_title' => $position->name,
                'post_content' => '',
                'post_status' => 'publish',
                'post_type' => $this->uow->position_repository->get_post_type(),
                'comment_status' => 'closed',
            );
            $inserted_id = wp_insert_post($position_post_data);
            if (!$inserted_id) {
                continue;
            }

            $this->uow->position_repository->update_project($inserted_id, $project_to);
            $this->uow->position_repository->copy($inserted_id, $position->id);
        }
    }

    public function update_name(int $project_id, int $project_name_id)
    {
        update_post_meta($project_id, 'summary_project_occurrence_name_id', $project_name_id);
    }

    public function update_time(int $project_id, string $occurrence)
    {
        update_post_meta($project_id, 'summary_project_occurrence_time', $occurrence);
    }

    public function update_meta(int $post_id, int $project_id, string $occurrence)
    {
        $project_occurrence_post = get_post($post_id);

        $project = get_post($project_id);
        $project_name = '';

        if ($project) {
            $project_name = $project->post_title;
        }
        if ($project_name && $occurrence) {

            $same_project = self::get_first_by_title("$project_name $occurrence");

            if ($same_project && $same_project->id != $post_id) {
                $project_occurrence_post->post_status = 'draft';
                $occurrence .= ' draft';
            }

            $project_occurrence_post->post_title = "$project_name $occurrence";
            $project_occurrence_post->post_name = preg_replace('/[^a-z0-9]+/', '_', strtolower("$project_name-$occurrence"));
        } else {
            $project_occurrence_post->post_status = 'draft';
        }

        $this->update_name($post_id, $project_id);
        $this->update_time($post_id, $occurrence);

        wp_update_post($project_occurrence_post);
    }

    protected function get_mapper(): Closure
    {
        return function (WP_Post $post) {
            return new Project($post->ID, $post->post_title);
        };
    }
}