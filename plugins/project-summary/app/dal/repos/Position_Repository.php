<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Post_Repository::class);
import_class(Position::class);
import_class(Position_Old::class);
import_class(Array_Util::class);

class Position_Repository extends Base_Post_Repository
{
    #[Pure] public function __construct(UoW $uoW)
    {
        parent::__construct(POSITION_POST_TYPE, $uoW);
    }

    /**
     * Checks whether given position belongs to given user.
     * @param int $id
     * @param int $user_id
     * @return bool
     */
    public function user_can_edit(int $id, int $user_id): bool
    {
        return $this->uow->user_repository->get_user_id_by_position($id) == $user_id;
    }

    /**
     * Checks, whether current user can view given position.
     * Returns true, when user has a same project of same series as given project id.
     *
     * @param int $position_id
     * @param int $user_id
     * @return bool
     */
    public function user_can_view(int $position_id, int $user_id): bool
    {
        $project = $this->uow->project_repository->get_first_by_position($position_id);

        $visible_project_names = $this->uow->project_name_repository->get_all_visible($user_id);

        foreach ($visible_project_names as $current_project_name) {
            foreach ($current_project_name->projects as $current_project) {
                if ($current_project->id == $project->id) {
                    return true;
                }
            }
        }
        return false;
    }

    public function is_locked(int $position_id): bool
    {
        return $this->time_for_position_hiding_remaining($position_id) < 0;
    }

    public function has_file_with_title(int $position_id, string $file_title): bool
    {
        $files = $this->uow->file_repository->get_all_by_position($position_id);
        foreach ($files as $file) {
            if ($file->title == $file_title) {
                return true;
            }
        }
        return false;
    }

    public function summaries_are_valid(int $position_id): bool
    {
        $summaries = $this->uow->summary_repository->get_all_by_position($position_id);
        if (sizeof($summaries) == 0) {
            return false;
        }

        foreach ($summaries as $summary) {
            if (!$this->uow->summary_repository->summary_is_valid($summary)) {
                return false;
            }
        }


        return true;
    }

    public function has_summaries(int $position_id): bool
    {
        global $wpdb;
        $sql_prepare = $wpdb->prepare("SELECT EXISTS (SELECT * FROM $wpdb->postmeta
            WHERE meta_key = 'summary_summary_position' AND meta_value = '%s') AS Result;", $position_id);
        $res = $wpdb->get_results($sql_prepare, ARRAY_A);

        return $res[0]['Result'] == 1;
    }

    public function get_first_by_file(int $file_id): ?Position
    {
        $position_id = intval(get_post_meta($file_id, 'summary_file_position', true));

        return $this->get_first($position_id);
    }

    public function get_first_by_summary(int $summary_id): ?Position
    {
        $position_id = intval(get_post_meta($summary_id, 'summary_summary_position', true));
        return $this->get_first($position_id);
    }

    public function get_complete_date_stamp(int $position_id): int
    {
        return intval(get_post_meta($position_id, 'summary_position_stamp', true));
    }

    public function get_meta(int $position_id): string
    {
        return strval(get_post_meta($position_id, 'summary_position_meta', true));
    }

    /**
     * @return array<string>
     */
    public function get_all_as_names(): array
    {
        $positions = $this->get_as_posts();
        $res = array();
        foreach ($positions as $position) {
            if (!in_array($position->post_title, $res)) {
                $res[] = $position->post_title;
            }
        }

        return $res;
    }

    /**
     * @param int $user_id
     * @return array<int>
     */
    public function get_position_ids_by_user(int $user_id): array
    {
        global $wpdb;
        $positions_prepare = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta
            WHERE meta_key = 'summary_position_user' AND meta_value = '%s'", $user_id);
        $rows = $wpdb->get_results($positions_prepare, ARRAY_A);
        $res = array();
        foreach ($rows as $row) {
            $res[] = $row['post_id'];
        }

        return $res;
    }

    /**
     * @return array<int>
     */
    public function get_position_ids_without_project(): array
    {
        global $wpdb;
        $rows = $wpdb->get_results("
            SELECT t2.post_id AS ids FROM $wpdb->postmeta t2
                LEFT JOIN $wpdb->posts t1 on t2.meta_value = t1.ID
            WHERE t2.meta_key = 'summary_position_project'
              AND t1.ID IS NULL", ARRAY_A);

        $res = array();
        foreach ($rows as $row) {
            $res[] = $row['ids'];
        }

        return $res;
    }

    /**
     * @param int $project_id
     * @return array<int>
     */
    public function get_position_ids_by_project(int $project_id): array
    {
        global $wpdb;
        $positions_prepare = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta
            WHERE meta_key = 'summary_position_project' AND meta_value = '%s'", $project_id);
        $rows = $wpdb->get_results($positions_prepare, ARRAY_A);
        $res = array();
        foreach ($rows as $row) {
            $res[] = $row['post_id'];
        }

        return $res;
    }

    /**
     * @param int $project_id
     * @return array<Position>
     */
    public function get_all_by_project(int $project_id): array
    {
        $res = array();

        $ids = $this->get_position_ids_by_project($project_id);

        foreach ($ids as $row) {
            $position = $this->get_first($row);

            if (!$position) {
                continue;
            }

            $res[] = $position;
        }

        return $res;
    }

    public function reset_timer(int $position_id)
    {
        if (!$this->exists($position_id)) {
            return;
        }
        if ($this->summaries_are_valid($position_id)) {
            $this->set_complete($position_id);
        } else {
            $this->set_not_complete($position_id);
        }
    }

    public function set_not_complete(int $position_id)
    {
        if (!$this->exists($position_id)) {
            return;
        }
        delete_post_meta($position_id, 'summary_position_stamp');
    }

    public function set_complete(int $position_id)
    {
        $this->update_complete_stamp($position_id, time());
    }

    private function update_complete_stamp(int $position_id, int $stamp)
    {
        if (!$this->exists($position_id)) {
            return;
        }
        update_post_meta($position_id, 'summary_position_stamp', $stamp);
    }

    public function update_project(int $position_id, int $project_id)
    {
        if (!$this->exists($position_id)) {
            return;
        }
        update_post_meta($position_id, 'summary_position_project', $project_id);
    }

    public function update_user(int $position_id, int $user_id)
    {
        if (!$this->exists($position_id)) {
            return;
        }
        update_post_meta($position_id, 'summary_position_user', $user_id);
    }

    public function update_meta(int $position_id, string $meta)
    {
        if (!$this->exists($position_id)) {
            return;
        }
        update_post_meta($position_id, 'summary_position_meta', $meta);
    }

    public function summary_updated(int $position_id)
    {
        if (!$this->exists($position_id)) {
            return;
        }

        $is_complete = true;
        $summaries = $this->uow->summary_repository->get_all_by_position($position_id);
        $summary_content_min_len = $this->uow->general_repository->get_summary_min_length();

        foreach ($summaries as $summary) {

            if (strlen($summary->content) < $summary_content_min_len) {
                $is_complete = false;
            }
        }

        if ($is_complete) {
            $this->uow->position_repository->set_complete($position_id);
            return;
        }

        $this->uow->position_repository->set_not_complete($position_id);
    }

    public function copy(int $position_to, int $position_from)
    {
        $summaries = $this
            ->uow
            ->summary_repository
            ->get_all_by_position($position_from);

        foreach ($summaries as $summary) {

            //TODO logic to base_repo
            $summary_post_data = array(
                'post_title' => $summary->heading,
                'post_content' => '',
                'post_status' => 'publish',
                'post_type' => $this->uow->summary_repository->get_post_type(),
                'comment_status' => 'closed',
            );

            $inserted_id = wp_insert_post($summary_post_data);
            if (!$inserted_id) {
                $errors[] = "Could not create a summary object with title '$summary->heading'!";
                continue;
            }

            $this->uow->summary_repository->update_clarification($inserted_id, $summary->clarification);
            $this->uow->summary_repository->update_position($inserted_id, $position_to);
        }
    }

    public function get_first(int $id): ?Position
    {
        return $this->get_as_entity($id);
    }

    /**
     * @param int $user_id
     * @return array<Position>
     */
    public function get_all_by_user(int $user_id): array
    {
        $params = array();
        if ($user_id != 0) {
            $params = array(
                'meta_key' => 'summary_position_user',
                'meta_value' => $user_id
            );
        }


        return $this->get_all_as_entity($params);
    }

    /**
     * @param int $user_id
     * @return array<Position_Old>
     */
    public function get_all_visible_to_user(int $user_id): array
    {
        if ($this->uow->user_repository->is_admin($user_id)) {
            $all = $this->get_all();
            return Entity_Mapper::map_all($all, function (Position $position) {
                return new Position_Old(
                    $position->id,
                    $position->name,
                    $position->project_id,
                    $position->user_id,
                    -1
                );
            });
        }

        $res = array();

        //find all projects where user is in and get positions from them.
        $projects = $this->uow->project_repository->get_all_by_user($user_id);

        foreach ($projects as $project) {
            $positions = $this->get_all_by_project($project->id);
            foreach ($positions as $position) {
                $position_old = new Position_Old(
                    $position->id,
                    $position->name,
                    $position->project_id,
                    $position->user_id,
                    -1
                );

                if (!Array_Util::in_array($position_old, $res)) {
                    $res[] = $position_old;
                }
            }
        }

        //get all users positions, where user has been or currently is
        $user_positions = $this->get_all_by_user($user_id);
        foreach ($user_positions as $user_position) {
            $time_to_hide_remaining = $this->time_for_old_position_removal_remaining($user_position->id);

            //filter out positions, that can not be seen
            if ($time_to_hide_remaining < 0) {
                continue;
            }

            $current_project = $this->uow->project_repository->get_first_by_position($user_position->id);
            if ($current_project == null) {
                continue;
            }
            $project_name = $this->uow->project_name_repository->get_first_by_project($current_project->id);
            $projects = $this->uow->project_repository->get_all_by_project_name($project_name->id);
            foreach ($projects as $project) {
                $positions = $this->get_all_by_project($project->id);
                foreach ($positions as $position) {
                    $position_old = new Position_Old(
                        $position->id,
                        $position->name,
                        $position->project_id,
                        $position->user_id,
                        $time_to_hide_remaining
                    );

                    if (!Array_Util::in_array($position_old, $res)) {
                        $res[] = $position_old;
                    }
                }
            }
        }

        return $res;
    }

    public function time_for_old_position_removal_remaining(int $position_id): int
    {
        $position = $this->get_first($position_id);
        if ($position == null) {
            return -1;
        }

        if ($position->completed_at == 0) {
            return 0;
        }

        $time_remaining_setting = $this->uow->general_repository->get_old_project_removal_time_in_seconds();
        $time_until = $position->completed_at + $time_remaining_setting;

        return $time_until - time();
    }

    public function time_for_position_hiding_remaining(int $position_id): int
    {
        $position = $this->get_first($position_id);
        if ($position == null) {
            return -1;
        }

        if ($position->completed_at == 0) {
            return 0;
        }

        $time_remaining_setting = $this->uow->general_repository->get_project_hiding_time_in_seconds();
        $time_until = $position->completed_at + $time_remaining_setting;

        return $time_until - time();
    }

    public function get_all(): array
    {
        return $this->get_all_by_user(0);
    }

    protected function get_mapper(): Closure
    {
        return function (WP_Post $post): Position {
            $meta_user_id = $this->uow->user_repository->get_user_id_by_position($post->ID);

            $project = $this->uow->project_repository->get_first_by_position($post->ID);
            $completed_at = $this->get_complete_date_stamp($post->ID);
            $meta = $this->get_meta($post->ID);
            return new Position(
                $post->ID,
                $post->post_title,
                $project != null ? $project->id : 0,
                $meta_user_id,
                $completed_at,
                $meta);
        };
    }
}