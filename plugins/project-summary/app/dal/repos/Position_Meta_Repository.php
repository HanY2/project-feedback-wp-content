<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Post_Repository::class);
import_class(Position_Meta::class);
import_class(Array_Util::class);

class Position_Meta_Repository extends Base_Post_Repository
{
    #[Pure] public function __construct(UoW $uow)
    {
        parent::__construct(POSITION_POST_TYPE, $uow);
    }

    public function get_first(int $id): ?Position_Meta
    {
        return $this->get_as_entity($id);
    }

    public function get_first_by_file(int $file_id): ?Position_Meta
    {
        $position_id = intval(get_post_meta($file_id, 'summary_file_position', true));

        return $this->get_first($position_id);
    }

    public function get_first_by_project_and_position_name(int $project_id, string $name): ?Position_Meta
    {
        $positions = $this->uow->position_repository->get_all_by_project($project_id);
        foreach ($positions as $position) {
            if ($position->name == $name) {
                return $this->get_first($position->id);
            }
        }
        return null;
    }

    public function get_first_by_summary(int $summary_id): ?Position_Meta
    {
        $position_id = intval(get_post_meta($summary_id, 'summary_summary_position', true));
        return $this->get_first($position_id);
    }

    public function get_previous(int $position_id): ?Position_Meta
    {
        /*
         * Given a list of projects of same series sorted by project name where given $summary_id belongs to,
         * find first project with an index that is smaller as current project but as large as possible
         * and has a position with same name as current.
         * Return the found position.
         *
         * Basically finds a position of same project that was organised before current project.
        */

        $project = $this->uow->project_repository->get_first_by_position($position_id);
        if ($project == null) {
            return null;
        }

        $projects = $this->uow->project_repository->get_all_of_same_project_name_by_position($position_id);

        $index = Array_Util::index_of($project, $projects);
        $select_index = $index - 1;

        $current_position = $this->uow->position_repository->get_first($position_id);

        if ($index > 0) {

            do {
                $before = $projects[$select_index];
                $result = $this->get_first_by_project_and_position_name($before->id, $current_position->name);
                $select_index--;

            } while ($select_index >= 0 && $result == null);


            return $result;
        }

        return null;
    }

    public function get_next(int $position_id): ?Position_Meta
    {
        /*
         * Given a list of projects of same series sorted by project name where given $summary_id belongs to,
         * find first project with an index that is bigger as current project but as small as possible
         * and has a position with same name as current.
         * Return the found position.
         *
         * Basically finds a position of same project that was organised after current project.
        */

        $project = $this->uow->project_repository->get_first_by_position($position_id);
        if ($project == null) {
            return null;
        }

        $projects = $this->uow->project_repository->get_all_of_same_project_name_by_position($position_id);

        $index = Array_Util::index_of($project, $projects);
        $select_index = $index + 1;

        $current_position = $this->uow->position_repository->get_first($position_id);

        if ($index + 1 < sizeof($projects)) {

            do {
                $before = $projects[$select_index];
                $result = $this->get_first_by_project_and_position_name($before->id, $current_position->name);
                $select_index++;

            } while ($select_index < sizeof($projects) && $result == null);

            return $result;
        }

        return null;
    }

    /**
     * @param int $include
     * @return array<Position_Meta>
     */
    public function get_all_where_timer_has_not_elapsed(int $include): array
    {
        global $wpdb;

        $prepared = $wpdb->prepare("
            SELECT t1.ID FROM $wpdb->posts t1
                LEFT JOIN (SELECT * FROM $wpdb->postmeta WHERE meta_key = 'summary_position_stamp')
                    t2 ON t1.ID = t2.post_id
            WHERE t1.post_type = '{$this->get_post_type()}'
              AND (t2.meta_key IS NULL 
                       OR t1.ID = %s 
                       OR (t2.meta_value > %s))
            ORDER BY t1.post_title",
            $include,
            time() - $this->uow->general_repository->get_project_hiding_time_in_seconds()
        );

        $res = $wpdb->get_results($prepared, ARRAY_A);
        $ids = array();
        foreach ($res as $item) {
            $ids[] = $item['ID'];
        }

        return $this->get_all(array(
            'post__in' => $ids,
        ));
    }

    /**
     * @param array $params
     * @return array<Position_Meta>
     */
    public function get_all(array $params = array()): array
    {
        return $this->get_all_as_entity($params);
    }

    /**
     * @param int $user_id
     * @param bool $include_finished
     * @return array<Position_Meta>
     */
    public function get_all_by_user(int $user_id, bool $include_finished = true): array
    {
        $params = array(
            'meta_key' => 'summary_position_user',
            'meta_value' => $user_id
        );

        $positions = $this->get_all_as_entity($params);

        return array_filter($positions, function (Position_Meta $position) use ($include_finished): bool {
            return $include_finished || $this->uow->position_repository->time_for_position_hiding_remaining($position->id) >= 0;
        });
    }

    protected function get_mapper(): Closure
    {
        return function (WP_Post $post) {
            $project = $this->uow->project_repository->get_first_by_position($post->ID);
            $user = $this->uow->user_repository->get_first_by_position($post->ID);
            $completed_at = $this->uow->position_repository->get_complete_date_stamp($post->ID);

            return new Position_Meta(
                $post->ID,
                $post->post_title,
                $project != null ? $project->id : 0,
                $user != null ? $user->ID : 0,
                $user != null ? $user->display_name : '',
                $project != null ? $project->name : '',
                $completed_at
            );
        };
    }
}