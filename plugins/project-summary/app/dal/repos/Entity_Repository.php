<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Entity_Repository::class);

class Entity_Repository extends Base_Entity_Repository
{
    #[Pure] public function __construct(UoW $uow)
    {
        parent::__construct($uow);
    }

    public function get_post_type(int $post_id): string|null
    {
        if ($this->exists($post_id)) {
            return get_post_type($post_id);
        }

        return null;
    }

    public function exists(int $id): bool
    {
        return get_post_status($id) == 'publish';
    }

    public function get_first(int $id): ?WP_Post
    {
        if ($this->exists($id)) {
            return get_post($id);
        }
        return null;
    }

    /**
     * @return array<WP_Post>
     */
    public function get_all(): array
    {
        return get_posts(array(
            'numberposts' => -1,
            'orderby' => 'title',
            'order' => 'ASC',
            'post_status' => 'publish',
        ));
    }
}