<?php

import_class(Base_Repository::class);

use JetBrains\PhpStorm\Pure;

class General_Repository extends Base_Repository
{
    #[Pure] public function __construct(UoW $uow)
    {
        parent::__construct($uow);
    }

    public function set_summary_message(string $message)
    {
        if (!get_option('summary_summary_form_message')) {
            add_option('summary_summary_form_message', $message);
        } else {
            update_option('summary_summary_form_message', $message);
        }
    }

    public function get_summary_message(): string
    {
        return sanitize_text_field(get_option('summary_summary_form_message'));
    }

    public function get_summary_min_length(): int
    {
        return intval(get_option('summary_min_length'));
    }

    public function set_summary_min_length(int $len)
    {
        if (!get_option('summary_min_length')) {
            add_option('summary_min_length', $len);
        } else {
            update_option('summary_min_length', $len);
        }
    }

    public function get_summary_max_length(): int
    {
        return intval(get_option('summary_max_length'));
    }

    public function set_summary_max_length(int $len)
    {
        if (!get_option('summary_max_length')) {
            add_option('summary_max_length', $len);
        } else {
            update_option('summary_max_length', $len);
        }
    }

    public function get_max_upload_file_size(): int
    {
        return intval(get_option('summary_max_file_size'));
    }

    public function set_max_upload_file_size(int $size)
    {
        if (!get_option('summary_max_file_size')) {
            add_option('summary_max_file_size', $size);
        } else {
            update_option('summary_max_file_size', $size);
        }
    }

    public function get_allowed_file_formats_as_string(): string
    {
        return sanitize_textarea_field(get_option('summary_allowed_file_formats'));
    }

    /**
     * @return array<string>
     */
    public function get_allowed_file_formats(): array
    {
        return explode(',', self::get_allowed_file_formats_as_string());
    }

    /**
     * @param array<string> $formats
     * @return void
     */

    public function set_allowed_file_formats(array $formats)
    {
        $output = implode(',', $formats);

        if (!get_option('summary_allowed_file_formats')) {
            add_option('summary_allowed_file_formats', $output);
        } else {
            update_option('summary_allowed_file_formats', $output);
        }
    }

    public function get_old_project_removal_time_seconds(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_remove_old_seconds')));
    }

    public function get_old_project_removal_time_minutes(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_remove_old_minutes')));
    }

    public function get_old_project_removal_time_hours(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_remove_old_hours')));
    }

    public function get_old_project_removal_time_days(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_remove_old_days')));
    }

    public function get_old_project_removal_time_in_seconds(): int
    {
        return
            $this->get_old_project_removal_time_days() * 24 * 60 * 60 +
            $this->get_old_project_removal_time_hours() * 60 * 60 +
            $this->get_old_project_removal_time_minutes() * 60 +
            $this->get_old_project_removal_time_seconds();
    }

    public function get_project_hiding_time_seconds(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_hide_seconds')));
    }

    public function get_project_hiding_time_minutes(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_hide_minutes')));
    }

    public function get_project_hiding_time_hours(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_hide_hours')));
    }

    public function get_project_hiding_time_days(): int
    {
        return intval(sanitize_text_field(get_option('summary_time_to_hide_days')));
    }

    public function get_project_hiding_time_in_seconds(): int
    {
        return
            $this->get_project_hiding_time_days() * 24 * 60 * 60 +
            $this->get_project_hiding_time_hours() * 60 * 60 +
            $this->get_project_hiding_time_minutes() * 60 +
            $this->get_project_hiding_time_seconds();
    }

    public function set_project_removal_time_seconds(int $time)
    {
        if (!get_option('summary_time_to_hide_seconds')) {
            add_option('summary_time_to_hide_seconds', $time);
        } else {
            update_option('summary_time_to_hide_seconds', $time);
        }
    }

    public function set_project_removal_time_minutes(int $time)
    {
        if (!get_option('summary_time_to_hide_minutes')) {
            add_option('summary_time_to_hide_minutes', $time);
        } else {
            update_option('summary_time_to_hide_minutes', $time);
        }
    }

    public function set_project_removal_time_hours(int $time)
    {
        if (!get_option('summary_time_to_hide_hours')) {
            add_option('summary_time_to_hide_hours', $time);
        } else {
            update_option('summary_time_to_hide_hours', $time);
        }
    }

    public function set_project_removal_time_days(int $time)
    {
        if (!get_option('summary_time_to_hide_days')) {
            add_option('summary_time_to_hide_days', $time);
        } else {
            update_option('summary_time_to_hide_days', $time);
        }
    }
}
