<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Post_Repository::class);
import_class(Project_Name::class);
import_class(Project_Old::class);
import_class(Project_Name_Old::class);

class Project_Name_Repository extends Base_Post_Repository
{

    #[Pure] public function __construct(UoW $uow)
    {
        parent::__construct(PROJECT_NAME_POST_TYPE, $uow);
    }

    /**
     * @param int $user_id
     * @return array<Project_Name_Old>
     */
    public function get_all_visible(int $user_id): array
    {
        /** @var array<Project_Name_Old> $project_names */
        $project_names = array();
        /** @var array<int, Project_Old> $projects */
        $projects = array();
        $visible_positions = $this->uow->position_repository->get_all_visible_to_user($user_id);

        foreach ($visible_positions as $position) {
            $project = $this->uow->project_repository->get_first_by_position($position->id);

            $project_old = $projects[$project->id] ?? new Project_Old($project->id, $project->name);

            if (!isset($projects[$project->id])) {
                $projects[$project_old->id] = $project_old;
            }

            $projects[$project_old->id]->positions[] = $position;

            if ($project_old->time_to_hide == 0) {
                $project_old->time_to_hide = $position->time_to_hide;
            }

        }

        foreach ($projects as $id => $project) {
            $project_name = $this->get_first_by_project($id);
            $project_name_old = $project_names[$project_name->id] ?? new Project_Name_Old($project_name->id, $project_name->name);

            if (!isset($project_names[$project_name->id])) {
                $project_names[$project_name->id] = $project_name_old;
            }

            $project_names[$project_name->id]->projects[] = $project;
        }

        return $project_names;
    }

    public function get_first_by_project(int $project_id): ?Project_Name
    {
        $id = get_post_meta($project_id, 'summary_project_occurrence_name_id', true);
        if ($id) {
            return $this->get_first(intval($id));
        }
        return null;
    }

    public function get_first(int $id): ?Project_Name
    {
        return $this->get_as_entity($id);
    }

    /**
     * @return array<Project_Name>
     */
    public function get_all(): array
    {
        return $this->get_all_as_entity();
    }

    protected function get_mapper(): Closure
    {
        return function (WP_Post $post) {
            return new Project_Name($post->ID, $post->post_title);
        };
    }
}