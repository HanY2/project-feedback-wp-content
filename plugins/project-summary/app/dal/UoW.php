<?php
import_class(Entity_Repository::class);
import_class(File_Repository::class);
import_class(General_Repository::class);
import_class(Position_Repository::class);
import_class(Position_Meta_Repository::class);
import_class(Project_Repository::class);
import_class(Project_Name_Repository::class);
import_class(Summary_Repository::class);
import_class(User_Repository::class);

class UoW
{
    public Entity_Repository $entity_repository;
    public File_Repository $file_repository;
    public General_Repository $general_repository;
    public Position_Repository $position_repository;
    public Position_Meta_Repository $position_meta_repository;
    public Project_Name_Repository $project_name_repository;
    public Project_Repository $project_repository;
    public Summary_Repository $summary_repository;
    public User_Repository $user_repository;

    public function __construct()
    {
        $this->entity_repository =
            $this->get_repo(Entity_Repository::class, function () {
                return new Entity_Repository($this);
            });
        $this->file_repository =
            $this->get_repo(File_Repository::class, function () {
                return new File_Repository($this);
            });
        $this->general_repository =
            $this->get_repo(General_Repository::class, function () {
                return new General_Repository($this);
            });
        $this->position_repository =
            $this->get_repo(Position_Repository::class, function () {
                return new Position_Repository($this);
            });
        $this->position_meta_repository =
            $this->get_repo(Position_Meta_Repository::class, function () {
                return new Position_Meta_Repository($this);
            });
        $this->project_name_repository =
            $this->get_repo(Project_Name_Repository::class, function () {
                return new Project_Name_Repository($this);
            });
        $this->project_repository =
            $this->get_repo(Project_Repository::class, function () {
                return new Project_Repository($this);
            });
        $this->summary_repository =
            $this->get_repo(Summary_Repository::class, function () {
                return new Summary_Repository($this);
            });
        $this->user_repository =
            $this->get_repo(User_Repository::class, function () {
                return new User_Repository($this);
            });
    }

    /**
     * Array of dictionary style, where key is className and value is Repository instance
     * Serves as a cache for instantiated repos
     * @var array<string, Base_Repository>
     */
    private array $repo_cache = array();

    /**
     * Uses $this->repo_cache for storing instantiated repositories. Upon request checks, whether a repo
     * is instantiated, if not, constructs it and stores it in $this->repo_cache.
     * Keeps memory clean of repo instances that are not needed.
     * @param string $type Repo className
     * @param callable $creation_method a constructor function
     * @return mixed Repo of className set by @param $type
     */
    private function get_repo(string $type, callable $creation_method): mixed
    {
        if (key_exists($type, $this->repo_cache)) {
            return $this->repo_cache[$type];
        }

        $repo = $creation_method();
        $this->repo_cache[$type] = $repo;
        return $repo;
    }
}