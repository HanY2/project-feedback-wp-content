<?php

use JetBrains\PhpStorm\Pure;

import_class(Base_Repository::class);

abstract class Base_Entity_Repository extends Base_Repository
{
    #[Pure] public function __construct(UoW $uow)
    {
        parent::__construct($uow);
    }

    public abstract function exists(int $id): bool;

    public abstract function get_first(int $id): mixed;

    public abstract function get_all(): array;
}