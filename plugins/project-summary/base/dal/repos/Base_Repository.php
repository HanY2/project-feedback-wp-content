<?php

abstract class Base_Repository
{
    protected UoW $uow;

    public function __construct(UoW $uow)
    {
        $this->uow = $uow;
    }
}