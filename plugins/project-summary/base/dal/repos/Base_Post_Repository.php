<?php

use JetBrains\PhpStorm\Pure;

import_class(Entity_Mapper::class);
import_class(Base_Entity_Repository::class);


abstract class Base_Post_Repository extends Base_Entity_Repository
{
    protected string $post_type;

    #[Pure] public function __construct(string $post_type, UoW $uow)
    {
        $this->post_type = $post_type;
        parent::__construct($uow);
    }

    protected function get_as_post(int $post_id): ?WP_Post
    {
        if ($this->exists($post_id) && $this->is_public($post_id)) {
            return get_post($post_id);
        }

        return null;
    }

    /**
     * @param int $id
     * @return mixed
     */
    protected function get_as_entity(int $id): mixed
    {
        return Entity_Mapper::map($this->get_as_post($id), $this->get_mapper());
    }

    protected function get_all_as_entity(array $params = array()): array
    {
        return Entity_Mapper::map_all($this->get_as_posts($params), $this->get_mapper());
    }

    protected abstract function get_mapper(): Closure;

    /**
     * @return array<WP_Post>
     */
    protected function get_as_posts(array $params = array()): array
    {
        $params = array_merge($params, array(
            'numberposts' => -1,
            'post_type' => $this->post_type,
            'orderby' => 'title',
            'order' => 'ASC',
            'post_status' => 'publish',
        ));

        return get_posts($params);
    }

    protected function update_post_title(int $id, string $title)
    {
        if (!$this->exists($id)) {
            throw new RuntimeException("Post with id '$id' of type '$this->post_type' not found!");
        }

        $post = get_post($id);
        $post->post_title = $title;
        wp_update_post($post);
    }

    protected function update_content(int $id, string $content)
    {
        if (!$this->exists($id)) {
            throw new RuntimeException("Post with id '$id' of type '$this->post_type' not found!");
        }

        $post = get_post($id);
        $post->post_content = $content;
        wp_update_post($post);
    }

    public function exists(int $id): bool
    {
        $post = get_post($id);
        return $post && $post->post_type == $this->post_type;
    }

    public function is_public(int $post_id): bool
    {
        if ($this->exists($post_id)) {
            return get_post($post_id)->post_status == 'publish';
        }

        throw new RuntimeException("Post with id '$post_id' not found!");
    }

    protected function update_post_status(int $post_id, string $post_status)
    {
        if ($this->exists($post_id)) {
            $post = get_post($post_id);
            $post->post_status = $post_status;
            wp_update_post($post);
            return;
        }

        throw new RuntimeException("Post with id '$post_id' not found!");
    }

    public function get_post_type(): string
    {
        return $this->post_type;
    }
}