<?php

class Array_Util
{
    public static function in_array($needle, array $array): bool
    {
        $item = self::find_in_array($needle, $array, function (IComparable $a, IComparable $b) {
            return $a->equals($b);
        });
        return $item != null;
    }

    public static function find_in_array($needle, array $array, $filter)
    {
        foreach ($array as $item) {
            if ($filter($needle, $item)) {
                return $item;
            }
        }
        return null;
    }

    public static function index_of(IComparable $needle, array $array): int
    {
        foreach ($array as $key => $item) {
            if ($needle->equals($item)) {
                return $key;
            }
        }

        return -1;
    }
}