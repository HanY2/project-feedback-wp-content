<?php

class Misc_Util
{
    public static function format_bytes(int $bytes, $precision = 2): string
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }


    public static function generate_random_filename(string $ext): string
    {
        return 'file_' . self::generate_random_string(30) . '.' . $ext;
    }

    public static function generate_random_string(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function to_readable_filename(string $filename): string
    {
        $max_len = 35;
        $min_filename_len = 3;

        if (strlen($filename) < $max_len) {
            return $filename;
        }

        $parts = explode('.', $filename);
        $ext = end($parts);

        $filename_rework = implode('.', $parts);

        $res = '';

        array_pop($parts);

        if (strlen($ext) >= floor($max_len / 2) - $min_filename_len) {
            $new_name = substr($filename_rework, 0, floor($max_len / 2) + $min_filename_len);

            return $new_name . '... .' . substr($ext, 0, strlen($new_name) - $min_filename_len - 1) . '...';
        }

        if (strlen($filename_rework) + strlen($ext) > $max_len) {
            $new_name = substr($filename_rework, 0, $max_len - strlen($ext));
            return $new_name . '... .' . $ext;
        }

        return $res;
    }
}