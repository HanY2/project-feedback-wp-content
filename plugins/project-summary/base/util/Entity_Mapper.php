<?php

class Entity_Mapper
{
    public static function map(mixed $entity, Closure $mapper): mixed
    {
        if ($entity == null) {
            return null;
        }

        return $mapper($entity);
    }

    public static function map_all(array $entities, Closure $mapper, bool $ignore_nulls = true): array
    {
        $res = array();

        foreach ($entities as $row) {
            if ($row == null && $ignore_nulls) {
                continue;
            }

            $mapped = $mapper($row);
            if ($ignore_nulls && $mapped == null) {
                continue;
            }

            $res[] = $mapped;
        }

        return $res;
    }
}