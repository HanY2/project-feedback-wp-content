<?php

$importer = new Class_Importer(SUMMARY_ABS_PATH_PLUGIN_ROOT . 'base/util/better-importer/cache.csv', SUMMARY_ABS_PATH_PLUGIN_ROOT);

function import_class(string $classname) {
    global $importer;

    $importer->import($classname);
}


/**
 * Used for managing importing for class files. More readable and does not require managing filepaths.
 */
class Class_Importer
{
    private string $cache_file_path;
    private string $abs_path;
    private array $cache = array();
    private bool $was_cache_rebuilt;
    private array $imported = array();

    /**
     * @param string $abs_cache_file_path absolute path to cache file.
     * @param string $abs_path absolute path to project root.
     */
    public function __construct(string $abs_cache_file_path, string $abs_path)
    {
        $this->cache_file_path = $abs_cache_file_path;
        $this->abs_path = $abs_path;
        $this->was_cache_rebuilt = false;

        $this->init();
    }

    public function import(string $classname) {

        if (!key_exists($classname, $this->cache) || !is_file($this->cache[$classname])) {
            if (!$this->was_cache_rebuilt) {
                $this->build_cache();
                $this->was_cache_rebuilt = true;
            } else {
                throw new RuntimeException("Class $classname not found. Could not import!");
            }
        }

        if (!in_array($classname, $this->imported)) {
            $this->imported[] = $classname;
            require_once $this->cache[$classname];
        }

    }

    private function init() {
        if (!file_exists($this->cache_file_path)) {
            throw new RuntimeException("File does not exist: $this->cache_file_path");
        }

        if ($this->is_file_empty()) {
            $this->build_cache(); // build the contents of cache.
        } else {
            $this->was_cache_rebuilt = false;
            $this->load_cache();
        }
    }

    private function is_file_empty(): bool
    {
        $contents = file_get_contents($this->cache_file_path);
        return $contents === false || strlen($contents) == 0;
    }

    private function build_cache() {

        $res = self::get_class_files_from_path($this->abs_path);
        $str = '';
        foreach ($res as $classname => $path) {
            $str .= $classname . ',' . $path . PHP_EOL;
        }

        file_put_contents($this->cache_file_path, $str);
        $this->load_cache();
    }

    private function load_cache()
    {
        $this->cache = array();
        $contents = file_get_contents($this->cache_file_path);
        $contents = trim($contents);
        $rows = explode(PHP_EOL, $contents);
        foreach ($rows as $row) {
            $parts = explode(',', $row);
            if (sizeof($parts) != 2) {
                throw new RuntimeException("Unexpected cache contents!");
            }

            $this->add_to_cache($parts[0], $parts[1]);
        }
    }

    private function add_to_cache(string $classname, string $path_to_script) {
        $this->cache[$classname] = $path_to_script;
    }

    /**
     * @param $dir
     * @return array<string, string>
     */
    private static function get_class_files_from_path($dir): array {

        $dir = rtrim($dir, '/');
        $ffs = scandir($dir);

        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);

        // prevent empty ordered elements
        if (count($ffs) < 1)
            return array();

        $res = array();
        foreach($ffs as $ff){
            if (is_dir($dir.'/'.$ff)) {
                $res = array_merge($res, self::get_class_files_from_path($dir.'/'.$ff));
            }
            else {
                $name = "$dir/$ff";
                if (pathinfo($name, PATHINFO_EXTENSION) == 'php') {
                    if (ctype_upper(basename($name)[0])) {
                        $res[basename($name, '.php')] = $name;
                    }
                }
            }
        }

        return $res;
    }
}
