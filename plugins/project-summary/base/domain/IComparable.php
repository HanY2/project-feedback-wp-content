<?php

interface IComparable
{
    public function equals(mixed $other): bool;
}