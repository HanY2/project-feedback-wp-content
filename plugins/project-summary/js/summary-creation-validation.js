function getErrorDiv(errors) {
    let errorDiv = document.getElementsByClassName('custom_error_wrapper')[0];
    if (!errorDiv) {
        errorDiv = document.createElement('div');
        errorDiv.className = 'custom_error_wrapper';
    }

    errorDiv.innerText = '';
    for (let i = 0; i < errors.length; i++) {
        const par = document.createElement('p');
        par.innerText = errors[i];
        errorDiv.appendChild(par);
    }

    return errorDiv;
}

function validate(event) {
    const titleInput = document.getElementById('titlewrap').getElementsByTagName('input')[0];
    const positionInput = document.getElementById('summary_summary_position_element');
    const wrapper = document.getElementsByClassName('wrap')[0];
    const errors = [];

    if (titleInput === null || titleInput.tagName !== 'INPUT') {
        errors.push('Script error! Could not validate name input. Please reload the page!');
    }

    if (positionInput === null || positionInput.tagName !== 'SELECT') {
        errors.push('Script error! Could not validate position selection. Please reload the page!');
    }

    if (!positionInput?.selectedIndex) {
        errors.push('Please select a position!');
    }

    if (!titleInput?.value) {
        errors.push('Summary name is required!')
    }

    if (errors.length > 0) {
        event.preventDefault();
        event.stopPropagation();
        wrapper.insertBefore(getErrorDiv(errors), wrapper.firstChild);
    }
}

document.addEventListener('click', function (e) {
    if (e.target && e.target.id === 'publish' || e.target.id === 'save-post') {
        validate(e);
    }
});
