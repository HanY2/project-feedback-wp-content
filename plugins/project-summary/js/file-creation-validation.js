function getErrorDiv(errors) {
    let errorDiv = document.getElementsByClassName('custom_error_wrapper')[0];
    if (!errorDiv) {
        errorDiv = document.createElement('div');
        errorDiv.className = 'custom_error_wrapper';
    }

    errorDiv.innerText = '';
    for (let i = 0; i < errors.length; i++) {
        const par = document.createElement('p');
        par.innerText = errors[i];
        errorDiv.appendChild(par);
    }

    return errorDiv;
}

function validate(event) {
    const positionInput = document.getElementById('summary_file_position_select');
    const oldFileCheck = document.getElementById('old_file_check');
    const fileUploadInput = document.getElementById('admin_file_upload_element');
    const wrapper = document.getElementsByClassName('wrap')[0];
    const errors = [];

    if (positionInput === null || positionInput.tagName !== 'SELECT') {
        errors.push('Script error! Can not validate position selection. Please reload the page!');
    }

    if (fileUploadInput === null || fileUploadInput.tagName !== 'INPUT') {
        errors.push('Script error! Can not validate file selection. Please reload the page!');
    }

    if (!positionInput?.selectedIndex) {
        errors.push('Please select a position!');
    }

    console.log(oldFileCheck);
    console.log(fileUploadInput);

    if (oldFileCheck === null && fileUploadInput?.files.length === 0) {
        errors.push('please select a file!');
    }

    if (errors.length > 0) {
        event.preventDefault();
        event.stopPropagation();
        wrapper.insertBefore(getErrorDiv(errors), wrapper.firstChild);
    }
}

document.addEventListener('click', function (e) {
    if (e.target && e.target.id === 'publish' || e.target.id === 'save-post') {
        validate(e);
    }
});
