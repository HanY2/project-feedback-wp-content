function formatBytes(bytes, precision = 2) {
    let units = ['B', 'KB', 'MB', 'GB', 'TB'];

    bytes = Math.max(bytes, 0);
    let pow = Math.floor((bytes ? Math.log(bytes) : 0) / Math.log(1024));
    pow = Math.min(pow, units.length - 1);

    bytes /= Math.pow(1024, pow);

    return '' + bytes.toFixed(precision) + ' ' + units[pow];
}

let input = document.getElementById('size-input');
let output = document.getElementById('pretty-size');

if (input != null && output != null) {
    updateOutput();
    input.addEventListener('input', () => {
        updateOutput();
    });
}

function updateOutput() {
    output.innerText = formatBytes(input.value);
}
