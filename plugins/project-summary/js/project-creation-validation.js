function getErrorDiv(errors) {
    let errorDiv = document.getElementsByClassName('custom_error_wrapper')[0];
    if (!errorDiv) {
        errorDiv = document.createElement('div');
        errorDiv.className = 'custom_error_wrapper';
    }

    errorDiv.innerText = '';
    for (let i = 0; i < errors.length; i++) {
        const par = document.createElement('p');
        par.innerText = errors[i];
        errorDiv.appendChild(par);
    }

    return errorDiv;
}

function validate(event) {
    const timeInput = document.getElementById('project_time');
    const nameInput = document.getElementById('summary_project_name_selection');
    const wrapper = document.getElementsByClassName('wrap')[0];

    const errors = [];

    if (timeInput === null || timeInput.tagName !== 'INPUT') {
        errors.push('Script error! Could not validate time input. Please reload the page!');
    }

    if (nameInput === null || nameInput.tagName !== 'SELECT') {
        errors.push('Script error! Could not validate name input. Please reload the page!');
    }

    if (timeInput !== null && timeInput.value.length === 0) {
        errors.push('Time input can not be empty!');
    }
    if (nameInput !== null && !nameInput.selectedIndex) {
        errors.push('Please select project name!');
    }

    if (errors.length > 0) {
        event.preventDefault();
        event.stopPropagation();
        wrapper.insertBefore(getErrorDiv(errors), wrapper.firstChild);
    }
}

document.addEventListener('click', function (e) {
    if (e.target && e.target.id === 'publish' || e.target.id === 'save-post') {
        validate(e);
    }
});
