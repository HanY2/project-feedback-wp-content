const elements = document.getElementsByClassName("content-editor");
for (const editorElement of elements) {
    const editor = new SimpleMDE({
        element: editorElement,
        spellChecker: false
    });

    editor.render();
}


