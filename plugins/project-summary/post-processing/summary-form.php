<?php
/*
 * This file handles the summary post request, validation and database update.
 */

import_class(Summary_Post::class);

global $unit_of_work;

global $is_debug;
$is_debug = isset($_POST['debug']) && $unit_of_work->user_repository->current_is_admin();

summary_echo_debug('<br><strong>post:</strong><br>' . print_r($_POST, true));
summary_echo_debug('<br><strong>files:</strong><br>' . print_r($_FILES, true));

if (!isset($_POST['summary_form_nonce']) || !wp_verify_nonce($_POST['summary_form_nonce'], 'summary_summary')) {
    return;
}

if (!isset($_POST['position']) || !intval($_POST['position'])) {
    return;
}

if (!isset($_POST['item'])) {
    return;
}

$item = $_POST['item'];
$position_id = intval($_POST['position']);


$params = array(
    'position' => $position_id,
    'item' => $item
);

$references_meta = isset($_POST['meta-item-references']) ? sanitize_textarea_field($_POST['meta-item-references']) : '';
summary_echo_debug("<br><strong>References:</strong><br>$references_meta<br>");


//update references

$unit_of_work->position_repository->update_meta($position_id, $references_meta);

switch ($item) {
    case 'file':
        $file_errors = array();
        $files_to_delete = summary_get_file_deletes();
        $file_delete_err = $unit_of_work->file_repository->remove_files($files_to_delete, get_current_user_id());
        if ($file_delete_err !== true) {
            $file_errors[] = $file_delete_err;
        }
        $upload_status = $unit_of_work->file_repository->upload('file_upload', $position_id);
        if ($upload_status != null && $upload_status !== true) {
            $file_errors[] = $upload_status;
        }
        if ($file_errors) {
            $params['errors'] = $file_errors;
        }
        break;
    default:
        $summary = summary_get_summary();
        summary_echo_debug('<br><strong>summary:</strong><br>' . print_r($summary, true));
        $update_response = $unit_of_work->summary_repository->update_single($summary);
        summary_echo_debug('<br><strong>summaries update result:</strong><br>' . print_r($update_response, true));
        if ($update_response->errors){
            $params['errors'] = $update_response->errors;
        } else {
            $params['status'] = 1;
        }
        break;
}

summary_echo_debug('<br><strong>params:</strong><br>' . print_r($params, true));

$redirect_url = get_permalink(get_page_by_title('#project_summary#'));
$redirect_url = add_query_arg($params, $redirect_url);

summary_echo_debug("<br><strong>redirect:</strong><br><a href='$redirect_url'>$redirect_url</a>");

if (!$is_debug) {
    wp_redirect($redirect_url);
    exit();
}

/**
 * @return array<int>
 */
function summary_get_file_deletes(): array
{
    $res = array();

    foreach ($_POST as $key => $value) {
        if (!str_contains($key, 'remove-')) {
            continue;
        }

        $pos = strrpos($key, '-');

        if (!$pos || !is_numeric(substr($key, $pos + 1))) {
            continue;
        }

        $res[] = intval(substr($key, $pos + 1));
    }

    return $res;
}

function summary_get_summary(): ?Summary_Post
{
    global $unit_of_work;
    if (!isset($_POST['content'])) {
        return null;
    }

    return new Summary_Post(
        $_POST['item'],
        $unit_of_work->user_repository->get_current_user_id(),
        $_POST['position'],
        sanitize_textarea_field($_POST['content'])
    );
}

/**
 * @param Response $response
 * @return array array of ids, pointing to status and errors, or null if no errors.
 */
function summary_parse_response_to_query_params(Response $response): array
{
    $res = array();

    foreach ($response->results as $item) {
        $errors = sizeof($item->errors) != 0 ? $item->errors : null;
        $res[$item->id] = array(
            'status' => $item->status,
            'errors' => $errors
        );
    }

    if ($response->global_errors) {
        $res['all'] =
            array(
                'errors' => $response->global_errors,
            );
    }

    return $res;
}

function summary_array_merge(array $first, array $second): array
{
    $res = array();
    foreach ($first as $key => $value) {
        $res[$key] = $value;
    }
    foreach ($second as $key => $value) {
        $res[$key] = $value;
    }

    return $res;
}

function summary_echo_debug(string $msg)
{
    global $is_debug;

    if ($is_debug) {
        echo $msg;
    }
}
